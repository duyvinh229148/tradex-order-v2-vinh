import { Inject, Service } from 'typedi';
import { Errors, Kafka, Logger, Utils } from 'tradex-common';
import {
  DEFAULT_FETCH_COUNT_ORDER_HISTORY,
  DEFAULT_PAGE_SIZE,
  DEFAULT_EQUITY_SUB_NUMBER,
  SellBuyTypeEnum,
  SortTypeEnum,
  URI,
  DEFAULT_DERIVATIVES_SUB_NUMBER,
} from '../constants';
import config from '../config';
import { v4 as uuid } from 'uuid';
import OrderHistoryResponse from '../models/response/OrderHistoryResponse';
import IOrderHistoryRequest from '../models/request/IOrderHistoryRequest';
import { InjectRepository } from 'typeorm-typedi-extensions';
import { AccountInfoRepository } from '../repositories/AccountInfoRepository';
import { DailyProfitTotalRepository } from '../repositories/DailyProfitTotalRepository';
import AccountInfo from '../models/db/AccountInfo';
import DailyProfit from '../models/db/DailyProfit';
import { LessThan, EntityManager, getManager } from 'typeorm';
import DailyProfitTotal from '../models/db/DailyProfitTotal';
import IDailyProfitRequest from '../models/request/IDailyProfitRequest';
import DailyProfitResponse, {
  parseToDailyProfitResponse,
} from '../models/response/DailyProfitResponse';
import TuxAccountProfitLossResponse, {
  ProfitLossItem,
} from '../models/response/TuxAccountProfitLossResponse';
import ITuxAccountProfitLossRequest from '../models/request/ITuxAccountProfitLossRequest';
import { ISymbolInfo } from '../models/ISymbolInfo';
import RedisDao from '../daos/RedisDao';

@Service()
export default class DailyProfitService {
  @InjectRepository()
  private accountInfoRepo: AccountInfoRepository;

  @InjectRepository()
  private dailyProfitTotalRepo: DailyProfitTotalRepository;

  @Inject()
  private readonly redisDao: RedisDao;

  public static async QUERY_ORDER_HISTORY(
    username: string,
    accountNumber: string,
    subNumber: string,
    lastOrderDate: string,
    lastBranchCode: string,
    lastOrderNumber: string,
    lastMatchPrice: number,
    fromDate: string,
    toDate: string,
    fetchCount: number
  ): Promise<OrderHistoryResponse[]> {
    Logger.info(`---------- queryOrderHistory ---------------`);

    const orderHistoryRequest: IOrderHistoryRequest = {
      accountNumber: accountNumber,
      subNumber: subNumber,
      fetchCount: fetchCount,
      lastOrderDate: lastOrderDate,
      lastBranchCode: lastBranchCode,
      lastOrderNumber: lastOrderNumber,
      lastMatchPrice: lastMatchPrice,
      sortType: SortTypeEnum.ASC.valueOf(),
      fromDate: fromDate,
      toDate: toDate,
      headers: {
        token: {
          userData: {
            accountNumbers: [accountNumber],
            username: username,
          },
        },
      },
    };

    const message: Kafka.IMessage = await Kafka.getInstance().sendRequestAsync(
      uuid(),
      config.topic.tuxedo,
      URI.QUERY_ORDER_HISTORY,
      orderHistoryRequest
    );
    if (message.data.status != null) {
      Logger.error(
        `Query OrderHistory failed: ${accountNumber}/${subNumber}: ${JSON.stringify(
          message
        )}`
      );
      throw new Errors.GeneralError(message.data.status);
    } else {
      return message.data.data;
    }
  }

  public static async QUERY_STOCK_BALANCE(
    username: string,
    accountNumber: string,
    subNumber: string,
    bankCode: string,
    bankName: string,
    lastStockCode: string,
    fetchCount: number
  ): Promise<TuxAccountProfitLossResponse> {
    Logger.info(`---------- queryStockBalance ---------------`);
    const accountProfitLossReq: ITuxAccountProfitLossRequest = {
      accountNumber: accountNumber,
      subNumber: subNumber,
      fetchCount: fetchCount,
      bankCode: bankCode,
      bankName: bankName,
      lastStockCode: lastStockCode,
      headers: {
        token: {
          userData: {
            accountNumbers: [accountNumber],
            username: username,
          },
        },
      },
    };

    const message: Kafka.IMessage = await Kafka.getInstance().sendRequestAsync(
      uuid(),
      config.topic.tuxedo,
      URI.QUERY_ACCOUNT_PROFIT_LOSS,
      accountProfitLossReq
    );
    if (message.data.status != null) {
      Logger.error(
        `Query AccountProfitLoss failed: ${accountNumber}/${subNumber}: ${JSON.stringify(
          message
        )}`
      );
      throw new Errors.GeneralError(message.data.status);
    } else {
      return message.data.data;
    }
  }

  public static async QUERY_TODAY_ORDER_HISTORY(
    username: string,
    accountNumber: string,
    subNumber: string,
    date: Date
  ): Promise<OrderHistoryResponse[]> {
    let orderHistoryList: OrderHistoryResponse[] = [];
    const fromDate: string = Utils.formatDateToDisplay(date);
    const toDate: string = Utils.formatDateToDisplay(date);
    const lastOrderDate: string = Utils.formatDateToDisplay(date);
    let lastBranchCode: string = null;
    let lastOrderNumber: string = null;
    let lastMatchPrice: number = null;
    const fetchCount: number = DEFAULT_PAGE_SIZE;
    let next: boolean = true;
    while (next) {
      const orderHistoryListResponse: OrderHistoryResponse[] = await DailyProfitService.QUERY_ORDER_HISTORY(
        username,
        accountNumber,
        subNumber,
        lastOrderDate,
        lastBranchCode,
        lastOrderNumber,
        lastMatchPrice,
        fromDate,
        toDate,
        fetchCount
      );
      orderHistoryList = orderHistoryList.concat(orderHistoryListResponse);
      if (orderHistoryListResponse.length === fetchCount) {
        const lastOrderHistoryRecord: OrderHistoryResponse =
          orderHistoryListResponse[orderHistoryListResponse.length - 1];
        lastBranchCode = lastOrderHistoryRecord.branchCode;
        lastOrderNumber = lastOrderHistoryRecord.orderNumber;
        lastMatchPrice = lastOrderHistoryRecord.matchedPrice;
      } else {
        next = false;
      }
    }
    return orderHistoryList;
  }

  public static async QUERY_TODAY_STOCK_BALANCE(
    accountInfo: AccountInfo
  ): Promise<ProfitLossItem[]> {
    let response: ProfitLossItem[] = [];
    const accountNumber: string = accountInfo.accountNumber;
    const subNumber: string = accountInfo.subNumber;
    const username: string = accountInfo.username;
    const bankCode: string = accountInfo.bankCode;
    const bankName: string = accountInfo.bankName;
    const fetchCount: number = DEFAULT_PAGE_SIZE;
    let lastStockCode: string = null;
    let next: boolean = true;
    while (next) {
      const accountProfitLossResponse: TuxAccountProfitLossResponse = await DailyProfitService.QUERY_STOCK_BALANCE(
        username,
        accountNumber,
        subNumber,
        bankCode,
        bankName,
        lastStockCode,
        fetchCount
      );
      const profitLossItems: ProfitLossItem[] =
        accountProfitLossResponse.profitLossItems == null
          ? []
          : accountProfitLossResponse.profitLossItems;

      response = response.concat(profitLossItems);
      if (profitLossItems.length === fetchCount) {
        const lastRecord: ProfitLossItem =
          profitLossItems[ProfitLossItem.length - 1];
        lastStockCode = lastRecord.stockCode;
      } else {
        next = false;
      }
    }
    return response;
  }

  public static CALC_BALANCE_PROFIT(
    change: number,
    stockBalance: number
  ): number {
    return stockBalance * change;
  }

  public static CALC_SELL_PROFIT(
    stockInfo: ISymbolInfo,
    orderHistoryList: OrderHistoryResponse[]
  ): number {
    let profit: number = 0;
    for (let i = 0; i < orderHistoryList.length; i++) {
      const orderHistory: OrderHistoryResponse = orderHistoryList[i];
      if (orderHistory.sellBuyType === SellBuyTypeEnum.SELL.valueOf()) {
        profit +=
          orderHistory.matchedQuantity *
          (orderHistory.matchedPrice - stockInfo.referencePrice);
      }
    }
    return profit;
  }

  public static CALC_BUY_PROFIT(
    stockInfo: ISymbolInfo,
    orderHistoryList: OrderHistoryResponse[]
  ): number {
    let profit: number = 0;
    for (let i = 0; i < orderHistoryList.length; i++) {
      const orderHistory: OrderHistoryResponse = orderHistoryList[i];
      if (orderHistory.sellBuyType === SellBuyTypeEnum.BUY.valueOf()) {
        profit +=
          orderHistory.matchedQuantity *
          (stockInfo.referencePrice - orderHistory.matchedPrice);
      }
    }
    return profit;
  }

  public async calculateDailyProfitByJob() {
    Logger.info(`______ start calculateDailyProfitByJob`);
    const holidaysStr: string[] = config.holidays;
    const today: Date = new Date();
    today.setHours(today.getHours() + config.market_time_zone);
    const todayStr = Utils.formatDateToDisplay(today);
    if (Utils.isWeekend(today)) {
      Logger.warn('today is weekend');
      Logger.info(`______ finished calculateDailyProfitByJob`);
      return;
    }
    if (holidaysStr.indexOf(todayStr) !== -1) {
      Logger.warn('today is holiday');
      Logger.info(`______ finished calculateDailyProfitByJob`);
      return;
    }
    const accountInfoList: AccountInfo[] = await this.accountInfoRepo.findAll();
    for (let i = 0; i < accountInfoList.length; i++) {
      const accountInfo: AccountInfo = accountInfoList[i];
      Logger.info(
        `Calc daily profit for accountNumber/subNumber: ${accountInfo.accountNumber}/${accountInfo.subNumber}`
      );
      if (accountInfo.subNumber === DEFAULT_DERIVATIVES_SUB_NUMBER) {
        Logger.info(`Ignore Derivatives sub number`);
        continue;
      }

      const today: Date = new Date();
      const username: string = accountInfo.username;
      const accountNumber: string = accountInfo.accountNumber;
      const subNumber: string = accountInfo.subNumber;

      let orderHistoryList: OrderHistoryResponse[] = await DailyProfitService.QUERY_TODAY_ORDER_HISTORY(
        username,
        accountNumber,
        subNumber,
        today
      );
      Logger.info(
        `all order history before filter: ${JSON.stringify(orderHistoryList)}`
      );
      orderHistoryList = orderHistoryList.filter(
        (orderHistoryResponse: OrderHistoryResponse) => {
          return orderHistoryResponse.matchedQuantity > 0;
        }
      );
      Logger.info(
        `all order history after filter: ${JSON.stringify(orderHistoryList)}`
      );
      const stockBalanceList: ProfitLossItem[] = await DailyProfitService.QUERY_TODAY_STOCK_BALANCE(
        accountInfo
      );
      Logger.info(`all stockBalance: ${JSON.stringify(stockBalanceList)}`);
      // group order history by code
      const mapCodeOrderHistory: Object = orderHistoryList.reduce(
        (result: any, currentValue: OrderHistoryResponse) => {
          result[currentValue.stockCode] =
            result[currentValue.stockCode] == null
              ? []
              : result[currentValue.stockCode];
          result[currentValue.stockCode].push(currentValue);
          return result;
        },
        Object.create(null)
      );

      Logger.info(
        `mapCodeOrderHistory: ${JSON.stringify(
          mapCodeOrderHistory
        )}, stockCode: ${Object.keys(mapCodeOrderHistory)}`
      );

      // group stockBalance by code
      const mapCodeStockBalance: Object = stockBalanceList.reduce(
        (result: any, currentValue: ProfitLossItem) => {
          result[currentValue.stockCode] = currentValue.balanceQuantity;
          Logger.info(
            `stock: ${currentValue.stockCode} - balanceQuantity: ${currentValue.balanceQuantity}`
          );
          return result;
        },
        Object.create(null)
      );
      Logger.info(
        `mapCodeStockBalance: ${JSON.stringify(
          mapCodeStockBalance
        )}, stockCode: ${Object.keys(mapCodeStockBalance)}`
      );

      let stockCodeList: string[] = [];
      stockCodeList = stockCodeList.concat(Object.keys(mapCodeOrderHistory));
      stockCodeList = stockCodeList.concat(Object.keys(mapCodeStockBalance));
      stockCodeList = Array.from(new Set(stockCodeList));
      const dailyProfitTotal: DailyProfitTotal = await this.createDailyProfitTotal(
        accountNumber,
        subNumber,
        today
      );
      const dailyProfitDetailList: DailyProfit[] = [];
      for (let i = 0; i < stockCodeList.length; i++) {
        const stockCode: string = stockCodeList[i];
        const stockBalance: number =
          mapCodeStockBalance[stockCode] == null
            ? 0
            : mapCodeStockBalance[stockCode];
        const dailyProfit: DailyProfit = await this.createDailyProfitDetail(
          accountNumber,
          subNumber,
          stockCode,
          today,
          mapCodeOrderHistory[stockCode],
          stockBalance
        );
        if (dailyProfit != null) {
          dailyProfitDetailList.push(dailyProfit);
          dailyProfitTotal.updateByDailyProfitDetail(dailyProfit);
        }
      }

      await getManager().transaction(
        async (transactionalEntityManager: EntityManager) => {
          await transactionalEntityManager.save(dailyProfitTotal);
          await transactionalEntityManager.save(dailyProfitDetailList, {
            chunk: DEFAULT_FETCH_COUNT_ORDER_HISTORY,
          });
        }
      );
      Logger.info(`______ finished calculateDailyProfitByJob`);
    }
  }

  public async createDailyProfitDetail(
    accountNumber: string,
    subNumber: string,
    stockCode: string,
    date: Date,
    orderHistoryList: OrderHistoryResponse[],
    stockBalance: number
  ): Promise<DailyProfit> {
    const stockInfo: ISymbolInfo = await this.redisDao.getSymbolInfo(stockCode);
    if (stockInfo == null) {
      Logger.error(
        `Ignore CalculateDailyProfit ${stockCode} because of not found stockInfo`
      );
      return null;
    }
    Logger.info(
      `CalculateDailyProfit: stockCode: ${stockCode}, todayClosePrice: ${stockInfo.last}, yesterdayClosePrice: ${stockInfo.referencePrice}`
    );
    const dailyProfit: DailyProfit = this.calculateDailyProfit(
      stockInfo,
      stockBalance,
      orderHistoryList,
      date
    );
    dailyProfit.accountNumber = accountNumber;
    dailyProfit.subNumber = subNumber;
    return dailyProfit;
  }

  public calculateDailyProfit(
    stockInfo: ISymbolInfo,
    stockBalance: number,
    orderHistoryList: OrderHistoryResponse[],
    date: Date
  ): DailyProfit {
    let sellAmount: number = 0;
    let buyAmount: number = 0;
    let sellProfit: number = 0;
    let buyProfit: number = 0;
    let balanceProfit: number = 0;
    if (orderHistoryList != null && orderHistoryList.length > 0) {
      for (let i = 0; i < orderHistoryList.length; i++) {
        const orderHistory: OrderHistoryResponse = orderHistoryList[i];
        if (orderHistory.sellBuyType === SellBuyTypeEnum.SELL.valueOf()) {
          sellAmount += orderHistory.matchedAmount;
        } else {
          buyAmount += orderHistory.matchedAmount;
        }
      }

      sellProfit = DailyProfitService.CALC_SELL_PROFIT(
        stockInfo,
        orderHistoryList
      );

      buyProfit = DailyProfitService.CALC_BUY_PROFIT(
        stockInfo,
        orderHistoryList
      );
    }
    if (stockBalance > 0) {
      balanceProfit = DailyProfitService.CALC_BALANCE_PROFIT(
        stockInfo.change,
        stockBalance
      );
    }

    Logger.info(
      `Result CalculateDailyProfit stockCode: ${
        stockInfo.code
      }, date: ${Utils.formatDateToDisplay(
        date
      )}, sellProfit: ${sellProfit}, buyProfit: ${buyProfit}, balanceProfit: ${balanceProfit}`
    );
    const totalProfit = sellProfit + buyProfit + balanceProfit;

    const todayEvaluatedAmount = stockBalance * stockInfo.last;
    const yesterdayEvaluatedAmount = stockBalance * stockInfo.referencePrice;
    let profitRatio = 0;
    if (yesterdayEvaluatedAmount === 0 && todayEvaluatedAmount === 0) {
      Logger.error(
        `data error: both yesterdayEvaluatedAmount and todayEvaluatedAmount = 0`
      );
    } else {
      profitRatio =
        totalProfit /
        (yesterdayEvaluatedAmount === 0
          ? todayEvaluatedAmount
          : yesterdayEvaluatedAmount);
    }

    const dailyProfit: DailyProfit = new DailyProfit();
    dailyProfit.date = date;
    dailyProfit.stockCode = stockInfo.code;
    dailyProfit.sellAmount = sellAmount;
    dailyProfit.sellingProfit = sellProfit;
    dailyProfit.buyAmount = buyAmount;
    dailyProfit.buyingProfit = buyProfit;
    dailyProfit.profit = totalProfit;
    dailyProfit.balanceProfit = balanceProfit;
    dailyProfit.stockBalance = stockBalance;
    dailyProfit.todayEvaluatedAmount = todayEvaluatedAmount;
    dailyProfit.yesterdayEvaluatedAmount = yesterdayEvaluatedAmount;
    dailyProfit.profitRatio = profitRatio;
    dailyProfit.securitiesInfo = stockInfo;

    return dailyProfit;
  }
  public async createDailyProfitTotal(
    accountNumber: string,
    subNumber: string,
    date: Date
  ): Promise<DailyProfitTotal> {
    let totalProfit: number = 0;
    let totalBuyingAmount: number = 0;
    let totalSellingAmount: number = 0;
    let totalBuyingProfit: number = 0;
    let totalSellingProfit: number = 0;
    let totalStockBalanceProfit: number = 0;
    const lastDailyProfitTotal: DailyProfitTotal = await this.dailyProfitTotalRepo.findLastByAccountNumberAndSubNumber(
      accountNumber,
      subNumber
    );
    if (lastDailyProfitTotal != null) {
      totalProfit = lastDailyProfitTotal.totalProfit;
      totalBuyingAmount = lastDailyProfitTotal.totalBuyingAmount;
      totalSellingAmount = lastDailyProfitTotal.totalSellingAmount;
      totalBuyingProfit = lastDailyProfitTotal.totalBuyingProfit;
      totalSellingProfit = lastDailyProfitTotal.totalSellingProfit;
      totalStockBalanceProfit = lastDailyProfitTotal.totalStockBalanceProfit;
    }
    const dailyProfitTotal: DailyProfitTotal = new DailyProfitTotal();
    dailyProfitTotal.date = date;
    dailyProfitTotal.accountNumber = accountNumber;
    dailyProfitTotal.subNumber = subNumber;
    dailyProfitTotal.dailyProfit = 0;
    dailyProfitTotal.sellAmount = 0;
    dailyProfitTotal.sellingProfit = 0;
    dailyProfitTotal.buyAmount = 0;
    dailyProfitTotal.buyingProfit = 0;
    dailyProfitTotal.balanceProfit = 0;
    dailyProfitTotal.todayEvaluatedAmount = 0;
    dailyProfitTotal.yesterdayEvaluatedAmount = 0;
    dailyProfitTotal.totalProfit = totalProfit;
    dailyProfitTotal.profitRatio = 0;
    dailyProfitTotal.totalBuyingAmount = totalBuyingAmount;
    dailyProfitTotal.totalSellingAmount = totalSellingAmount;
    dailyProfitTotal.totalBuyingProfit = totalBuyingProfit;
    dailyProfitTotal.totalSellingProfit = totalSellingProfit;
    dailyProfitTotal.totalStockBalanceProfit = totalStockBalanceProfit;
    return dailyProfitTotal;
  }

  public async queryDailyProfit(
    request: IDailyProfitRequest
  ): Promise<DailyProfitResponse[]> {
    const invalidParams = new Errors.InvalidParameterError();
    Utils.validate(request.accountNumber, 'accountNumber')
      .setRequire()
      .throwValid(invalidParams);

    invalidParams.throwErr();
    if (Utils.isEmpty(request.subNumber)) {
      request.subNumber = DEFAULT_EQUITY_SUB_NUMBER;
    }
    if (Utils.isEmpty(request.baseDate)) {
      const date: Date = new Date();
      date.setDate(date.getDate() + 1);
      request.baseDate = Utils.formatDateToDisplay(date);
    }

    const baseDate = Utils.getStartOfDate(
      Utils.convertStringToDate(request.baseDate)
    );
    const filter: any = {
      accountNumber: request.accountNumber,
      subNumber: request.subNumber,
      date: LessThan(baseDate),
    };
    if (
      Utils.isNullOrUndefined(request.fetchCount) ||
      request.fetchCount <= 0
    ) {
      request.fetchCount = DEFAULT_PAGE_SIZE;
    }

    const sort: any = { date: 'DESC' };
    const dailyProfitTotalList: DailyProfitTotal[] = await this.dailyProfitTotalRepo.findBy(
      filter,
      request.fetchCount,
      sort
    );
    return dailyProfitTotalList.map(parseToDailyProfitResponse);
  }
}
