import { Service } from 'typedi';
import { LoginTuxedoResponse } from '../models/response/LoginTuxedoResponse';
import { AccountInfoRepository } from '../repositories/AccountInfoRepository';
import AccountInfo from '../models/db/AccountInfo';
import { Logger } from 'tradex-common';
import { EntityManager, getManager } from 'typeorm';
import { InjectRepository } from 'typeorm-typedi-extensions';
import {
  toAccountInfoList,
  toFssAccountInfoList,
} from '../utils/ResponseUtils';
import { ILoginFssResponse } from '../models/response/LoginFssResponse';

@Service()
export default class AccountInfoService {
  @InjectRepository()
  private accountInfoRepo: AccountInfoRepository;

  public async saveOrUpdateAccountInfo(
    loginTuxedoResponse: LoginTuxedoResponse
  ) {
    const accountInfoList: AccountInfo[] = toAccountInfoList(
      loginTuxedoResponse
    );
    await getManager().transaction(async (entityManager: EntityManager) => {
      for (let i = 0; i < accountInfoList.length; i++) {
        const accountInfo: AccountInfo = accountInfoList[i];
        const dbAccountInfo: AccountInfo = await this.accountInfoRepo.findOneByAccountNumberAndSubNumber(
          accountInfo.accountNumber,
          accountInfo.subNumber
        );
        if (dbAccountInfo != null) {
          accountInfo.id = dbAccountInfo.id;
          Logger.info(
            `update to existed: ${accountInfo.accountNumber}/${accountInfo.subNumber}`
          );
        }

        await entityManager.save(accountInfo);
      }
    });
  }

  public async saveOrUpdateFssAccountInfo(loginFssResponse: ILoginFssResponse) {
    const accountInfoList: AccountInfo[] = toFssAccountInfoList(
      loginFssResponse
    );
    await getManager().transaction(async (entityManager: EntityManager) => {
      for (let i = 0; i < accountInfoList.length; i++) {
        const accountInfo: AccountInfo = accountInfoList[i];
        const dbAccountInfo: AccountInfo = await this.accountInfoRepo.findOneByAccountNumberAndSubNumber(
          accountInfo.accountNumber,
          accountInfo.subNumber
        );
        if (dbAccountInfo != null) {
          accountInfo.id = dbAccountInfo.id;
          Logger.info(
            `update to existed: ${accountInfo.accountNumber}/${accountInfo.subNumber}`
          );
        }

        await entityManager.save(accountInfo);
      }
    });
  }
}
