import { Inject, Service } from 'typedi';
import { InjectRepository } from 'typeorm-typedi-extensions';
import { Errors, Kafka, Logger, Utils } from 'tradex-common';
import StopOrder from '../models/db/StopOrder';
import { StopOrderRepository } from '../repositories/StopOrderRepository';
import {
  DEFAULT_DATE,
  DEFAULT_PAGE_SIZE,
  DEFAULT_EQUITY_SUB_NUMBER,
  MARKET_TIMEZONE,
  MarketTypeEnum,
  SellBuyTypeEnum,
  STOP_ORDER_STATUS,
  StopOrderTypeEnum,
} from '../constants';
import {
  Between,
  EntityManager,
  FindConditions,
  getManager,
  In,
  LessThan,
  LessThanOrEqual,
  MoreThanOrEqual,
  Not,
} from 'typeorm';
import config from '../config';
import { ISymbolInfo } from '../models/ISymbolInfo';
import IStopOrderCancelAllRequest from '../models/request/IStopOrderCancelAllRequest';
import IStopOrderCancelRequest from '../models/request/IStopOrderCancelRequest';
import * as Ajv from 'ajv';
import {
  StopOrderPlaceRequest,
  StopOrderPlaceResponse,
  StopOrderModifyRequest,
  StopOrderModifyResponse,
  StopOrderHistoryRequest,
  StopOrderHistoryResponse,
  StopOrderCancelMultiRequest,
} from 'tradex-models-order';
import {
  stopOrderCancelMultiRequestValidator,
  stopOrderPlaceRequestValidator,
  stopOrderHistoryRequestValidator,
} from 'tradex-models-order-validator';
import {
  INVALID_PARAMETER,
  BUY_STOP_ORDER_PRICE_CANNOT_LTE_THAN_CURRENT_PRICE,
  SELL_STOP_PRICE_CANNOT_GTE_THAN_CURRENT_PRICE,
  TO_DATE_MUST_BE_GTE_THAN_FROM_DATE,
  FROM_DATE_INVALID,
} from '../constants/errors';
import {
  toStopOrderPlaceResponse,
  toStopOrderModifyResponse,
  toStopOrderHistoryResponse,
} from '../utils/ResponseUtils';
import { fromStopOrderPlaceRequest } from '../utils/RequestUtils';
import RedisDao from '../daos/RedisDao';
import IStopOrderActiveRequest from '../models/request/IStopOrderActiveRequest';
import { v4 as uuid } from 'uuid';

@Service()
export default class StopOrderService {
  @InjectRepository()
  private readonly stopOrderRepository: StopOrderRepository;

  @Inject()
  private readonly redisDao: RedisDao;

  // tslint:disable-next-line:cyclomatic-complexity
  public async placeStopOrder(
    request: StopOrderPlaceRequest
  ): Promise<StopOrderPlaceResponse> {
    const validator: Ajv.ValidateFunction = stopOrderPlaceRequestValidator();
    if (!validator(request)) {
      throw new Errors.GeneralError(INVALID_PARAMETER, validator.errors);
    }

    const currentMarketDate: Date = new Date();
    currentMarketDate.setHours(
      currentMarketDate.getHours() + config.market_time_zone
    );
    if (Utils.isEmpty(request.fromDate)) {
      request.fromDate = Utils.formatDateToDisplay(currentMarketDate);
    }
    if (Utils.isEmpty(request.toDate)) {
      request.toDate = Utils.formatDateToDisplay(currentMarketDate);
    }
    if (request.subNumber == null) {
      request.subNumber = DEFAULT_EQUITY_SUB_NUMBER;
    }
    if (request.orderType === StopOrderTypeEnum.STOP.valueOf()) {
      request.orderPrice = null;
    }

    const fromDate: Date = Utils.getStartOfDate(
      Utils.convertStringToDate(request.fromDate)
    );
    const toDate: Date = Utils.getEndOfDate(
      Utils.convertStringToDate(request.toDate)
    );
    toDate.setMilliseconds(0);

    if (Utils.compareDateOnly(fromDate, currentMarketDate) < 0) {
      throw new Errors.GeneralError(FROM_DATE_INVALID);
    }

    if (Utils.compareDateOnly(fromDate, toDate) > 0) {
      throw new Errors.GeneralError(TO_DATE_MUST_BE_GTE_THAN_FROM_DATE);
    }

    if (
      Utils.compareDateOnly(fromDate, currentMarketDate) === 0 &&
      currentMarketDate.getHours() >= 15
    ) {
      throw new Errors.GeneralError(FROM_DATE_INVALID);
    }

    const symbolInfo: ISymbolInfo = await this.redisDao.getSymbolInfo(
      request.stockCode
    );
    if (
      request.orderType === StopOrderTypeEnum.STOP_LIMIT.valueOf() &&
      symbolInfo != null &&
      symbolInfo.marketType !== MarketTypeEnum.UPCOM
    ) {
      const invalidParams = new Errors.InvalidParameterError();
      Utils.validate(request.orderPrice, 'orderPrice')
        .setRequire()
        .throwValid(invalidParams);
      invalidParams.throwErr();
    }
    if (
      symbolInfo != null &&
      Utils.compareDateOnly(currentMarketDate, fromDate) === 0
    ) {
      Logger.info(`stockInfo: ${JSON.stringify(symbolInfo)}`);
      if (
        request.sellBuyType === SellBuyTypeEnum.SELL.valueOf() &&
        request.stopPrice >= symbolInfo.last
      ) {
        throw new Errors.GeneralError(
          SELL_STOP_PRICE_CANNOT_GTE_THAN_CURRENT_PRICE
        );
      } else if (
        request.sellBuyType === SellBuyTypeEnum.BUY.valueOf() &&
        request.stopPrice <= symbolInfo.last
      ) {
        throw new Errors.GeneralError(
          BUY_STOP_ORDER_PRICE_CANNOT_LTE_THAN_CURRENT_PRICE
        );
      }
    }

    const conditions: FindConditions<StopOrder> = {
      stockCode: request.stockCode,
      stopPrice: request.stopPrice,
      sbType: request.sellBuyType,
      orderType: request.orderType,
      orderPrice: request.orderPrice == null ? null : request.orderPrice,
      accountNumber: request.accountNumber,
      bankCode: request.bankCode,
      bankAccount: request.bankAccount,
      subNumber: request.subNumber,
      status: STOP_ORDER_STATUS.PENDING,
    };
    const existedStopOrder: StopOrder[] = await this.stopOrderRepository.findBy(
      conditions
    );

    if (existedStopOrder.length > 0) {
      existedStopOrder.forEach((stopOrder: StopOrder) => {
        if (
          (fromDate >= stopOrder.fromDate && fromDate <= stopOrder.toDate) ||
          (toDate >= stopOrder.fromDate && toDate <= stopOrder.toDate)
        ) {
          Logger.warn(`AlreadyExistedError, id: ${stopOrder.id}`);
          throw new Errors.AlreadyExistedError();
        }
      });
    }

    const newStopOrder: StopOrder = fromStopOrderPlaceRequest(
      request,
      fromDate,
      toDate
    );

    await getManager().transaction(async (txEntityManager: EntityManager) => {
      await txEntityManager.save(newStopOrder);
    });

    if (
      currentMarketDate.getTime() >= fromDate.getTime() &&
      currentMarketDate.getTime() <= toDate.getTime()
    ) {
      // sync with market-monitor
      await Kafka.getInstance().sendRequestAsync(
        uuid(),
        config.topic.marketMonitor,
        '/order/stop/add',
        newStopOrder
      );
    }
    return toStopOrderPlaceResponse(newStopOrder);
  }

  // tslint:disable-next-line:cyclomatic-complexity
  public async modifyStopOrder(
    request: StopOrderModifyRequest
  ): Promise<StopOrderModifyResponse> {
    const validator: Ajv.ValidateFunction = stopOrderPlaceRequestValidator();
    if (!validator(request)) {
      throw new Errors.GeneralError(INVALID_PARAMETER, validator.errors);
    }

    const symbolInfo: ISymbolInfo = await this.redisDao.getSymbolInfo(
      request.stockCode
    );
    //validate price
    if (
      request.orderType === StopOrderTypeEnum.STOP_LIMIT.valueOf() &&
      symbolInfo != null &&
      symbolInfo.marketType !== MarketTypeEnum.UPCOM
    ) {
      const invalidParams = new Errors.InvalidParameterError();
      Utils.validate(request.orderPrice, 'orderPrice')
        .setRequire()
        .throwValid(invalidParams);
      invalidParams.throwErr();
    }

    if (request.orderType === StopOrderTypeEnum.STOP.valueOf()) {
      request.orderPrice = null;
      request.newOrderPrice = null;
    }

    const reqFromDate: Date =
      request.fromDate != null
        ? Utils.convertStringToDate(request.fromDate)
        : new Date();
    const reqToDate: Date =
      request.toDate != null
        ? Utils.convertStringToDate(request.toDate)
        : new Date();
    //find the exact stopOrder to modify
    const conditions: FindConditions<StopOrder> = {
      stockCode: request.stockCode,
      stopPrice: request.stopPrice,
      sbType: request.sellBuyType,
      orderType: request.orderType,
      orderPrice: request.orderPrice == null ? null : request.orderPrice,
      accountNumber: request.accountNumber,
      bankCode: request.bankCode,
      bankAccount: request.bankAccount,
      subNumber: request.subNumber,
      status: STOP_ORDER_STATUS.PENDING,
      fromDate: Between(
        Utils.getStartOfDate(reqFromDate),
        Utils.getEndOfDate(reqFromDate)
      ),
      toDate: Between(
        Utils.getStartOfDate(reqToDate),
        Utils.getEndOfDate(reqToDate)
      ),
    };
    const stopOrder: StopOrder = await this.stopOrderRepository.findOneBy(
      conditions
    );
    if (stopOrder == null) {
      throw new Errors.ObjectNotFoundError();
    }

    const newFromDate: Date =
      request.newFromDate != null
        ? Utils.getStartOfDate(Utils.convertStringToDate(request.newFromDate))
        : Utils.getStartOfDate(stopOrder.fromDate);

    const newToDate: Date =
      request.newToDate != null
        ? Utils.getEndOfDate(Utils.convertStringToDate(request.newToDate))
        : Utils.getEndOfDate(stopOrder.toDate);
    newToDate.setMilliseconds(0);

    //validate newFromDate, newToDate
    const currentMarketDate: Date = new Date();
    currentMarketDate.setHours(currentMarketDate.getHours() + MARKET_TIMEZONE);
    if (Utils.compareDateOnly(newFromDate, currentMarketDate) < 0) {
      throw new Errors.GeneralError(FROM_DATE_INVALID);
    }
    if (
      Utils.compareDateOnly(newFromDate, currentMarketDate) === 0 &&
      currentMarketDate.getHours() >= 15
    ) {
      throw new Errors.GeneralError(FROM_DATE_INVALID);
    }

    if (newFromDate > newToDate) {
      throw new Errors.GeneralError(TO_DATE_MUST_BE_GTE_THAN_FROM_DATE);
    }

    //if newFromDate is today, validate price
    if (
      symbolInfo != null &&
      request.newStopPrice != null &&
      Utils.compareDateOnly(currentMarketDate, newFromDate) === 0
    ) {
      Logger.info(`stockInfo: ${JSON.stringify(symbolInfo)}`);
      if (
        stopOrder.sbType === SellBuyTypeEnum.SELL.valueOf() &&
        request.newStopPrice >= symbolInfo.last
      ) {
        throw new Errors.GeneralError(
          'SELL_STOP_PRICE_CANNOT_GTE_THAN_CURRENT_PRICE'
        );
      } else if (
        stopOrder.sbType === SellBuyTypeEnum.BUY.valueOf() &&
        request.newStopPrice <= symbolInfo.last
      ) {
        throw new Errors.GeneralError(
          'BUY_STOP_ORDER_PRICE_CANNOT_LTE_THAN_CURRENT_PRICE'
        );
      }
    }

    let newOrderQuantity: number =
      request.orderQuantity == null
        ? stopOrder.quantity
        : request.orderQuantity;

    //find old, similar stopOrders that have the same sbType, prices, pending... with the new value of the modifying stopOrder
    const similarStopOrders: StopOrder[] = await this.stopOrderRepository.findBy(
      {
        id: Not(stopOrder.id),
        stockCode: request.stockCode,
        stopPrice: request.newStopPrice,
        sbType: request.sellBuyType,
        orderType: request.orderType,
        bankCode: request.bankCode,
        bankAccount: request.bankAccount,
        subNumber:
          request.subNumber == null
            ? DEFAULT_EQUITY_SUB_NUMBER
            : request.subNumber,
        orderPrice:
          request.newOrderPrice == null
            ? request.orderPrice
            : request.newOrderPrice,
        accountNumber: request.accountNumber,
        status: STOP_ORDER_STATUS.PENDING,
      }
    );

    const stopOrderCancelList: number[] = [];
    //put every changes in stopOrder in a transaction
    await getManager().transaction(
      async (transactionalEntityManager: EntityManager) => {
        if (similarStopOrders.length > 0) {
          for (const similarStopOrder of similarStopOrders) {
            // if already exist record with same newStopPrice, newOrderPrice, fromDate, toDate delete it and accumulate order quantity
            if (
              Utils.compareDateOnly(similarStopOrder.fromDate, newFromDate) ===
                0 &&
              Utils.compareDateOnly(similarStopOrder.toDate, newToDate) === 0
            ) {
              similarStopOrder.status = STOP_ORDER_STATUS.CANCELLED;
              newOrderQuantity += similarStopOrder.quantity;
              //cancel the old stopOrder
              await transactionalEntityManager.update(
                StopOrder,
                similarStopOrder.id,
                similarStopOrder
              );
              stopOrderCancelList.push(similarStopOrder.id);
            } else if (
              (newFromDate >= similarStopOrder.fromDate &&
                newFromDate <= similarStopOrder.toDate) ||
              (newToDate >= similarStopOrder.fromDate &&
                newToDate <= similarStopOrder.toDate)
            ) {
              Logger.warn(`AlreadyExistedError, id: ${similarStopOrder.id}`);
              throw new Errors.AlreadyExistedError();
            }
          }
        }
        //update stopOrder
        if (request.newStopPrice != null) {
          stopOrder.stopPrice = request.newStopPrice;
        }
        stopOrder.quantity = newOrderQuantity;
        if (request.newOrderPrice != null) {
          stopOrder.orderPrice = request.newOrderPrice;
        }
        stopOrder.fromDate = newFromDate;
        stopOrder.toDate = newToDate;
        await transactionalEntityManager.update(
          StopOrder,
          stopOrder.id,
          stopOrder
        );
      }
    );

    // sync with market-monitor
    if (stopOrderCancelList.length > 0) {
      await Kafka.getInstance().sendRequestAsync(
        uuid(),
        config.topic.marketMonitor,
        '/order/stop/cancel',
        stopOrderCancelList
      );
    }
    await Kafka.getInstance().sendRequestAsync(
      uuid(),
      config.topic.marketMonitor,
      '/order/stop/update',
      stopOrder
    );

    return toStopOrderModifyResponse();
  }

  public async cancelStopOrder(request: IStopOrderCancelRequest): Promise<any> {
    const invalidParams = new Errors.InvalidParameterError();
    Utils.validate(request.sequence, 'sequence')
      .setRequire()
      .throwValid(invalidParams);
    invalidParams.throwErr();

    const stopOrder: StopOrder = await this.stopOrderRepository.findOneBy({
      id: request.sequence,
      status: STOP_ORDER_STATUS.PENDING,
    });

    if (stopOrder == null) {
      throw new Errors.ObjectNotFoundError();
    }

    await getManager().transaction(
      async (transactionalEntityManager: EntityManager) => {
        await transactionalEntityManager.update(StopOrder, stopOrder.id, {
          status: STOP_ORDER_STATUS.CANCELLED,
          cancelledAt: new Date(),
        });
      }
    );

    const today: Date = new Date();
    if (
      Utils.compareDateOnly(today, stopOrder.fromDate) >= 0 &&
      Utils.compareDateOnly(today, stopOrder.toDate) <= 0
    ) {
      // sync with market-monitor
      await Kafka.getInstance().sendRequestAsync(
        uuid(),
        config.topic.marketMonitor,
        '/order/stop/cancel',
        [stopOrder.id]
      );
    }

    return {};
  }

  public async cancelMultiStopOrder(
    request: StopOrderCancelMultiRequest
  ): Promise<any> {
    const validator: Ajv.ValidateFunction = stopOrderCancelMultiRequestValidator();
    if (!validator(request)) {
      throw new Errors.GeneralError(INVALID_PARAMETER, validator.errors);
    }

    const listStopOrderPending: StopOrder[] = await this.stopOrderRepository.find(
      {
        id: In(request.orderIdList),
        status: STOP_ORDER_STATUS.PENDING,
      }
    );
    Logger.info(
      `listStopOrderPending: ${JSON.stringify(listStopOrderPending)}`
    );
    if (listStopOrderPending == null || listStopOrderPending.length === 0) {
      return {};
    }
    await getManager().transaction(
      async (transactionalEntityManager: EntityManager) => {
        for (let i = 0; i < listStopOrderPending.length; i++) {
          listStopOrderPending[i].status = STOP_ORDER_STATUS.CANCELLED;
          listStopOrderPending[i].cancelledAt = new Date();
        }
        await transactionalEntityManager.save(listStopOrderPending);
      }
    );

    // sync with market-monitor
    await Kafka.getInstance().sendRequestAsync(
      uuid(),
      config.topic.marketMonitor,
      '/order/stop/cancel',
      request.orderIdList
    );
    return {};
  }

  public async queryStopOrderHistory(
    request: StopOrderHistoryRequest
  ): Promise<StopOrderHistoryResponse[]> {
    const validator: Ajv.ValidateFunction = stopOrderHistoryRequestValidator();
    if (!validator(request)) {
      throw new Errors.GeneralError(INVALID_PARAMETER, validator.errors);
    }

    if (Utils.isEmpty(request.subNumber)) {
      request.subNumber = DEFAULT_EQUITY_SUB_NUMBER;
    }

    let fromDate = Utils.convertStringToDate(DEFAULT_DATE);
    let toDate = new Date();

    if (!Utils.isEmpty(request.fromDate)) {
      fromDate = Utils.convertStringToDate(request.fromDate);
    }

    if (!Utils.isEmpty(request.toDate)) {
      toDate = Utils.convertStringToDate(request.toDate);
    }

    fromDate = Utils.getStartOfDate(fromDate);
    toDate = Utils.getEndOfDate(toDate);

    const filter: FindConditions<StopOrder> = {
      userId: request.headers.token.userId,
      accountNumber: request.accountNumber,
      subNumber: request.subNumber,
    };

    if (request.sequence != null) {
      filter.id = LessThan(request.sequence);
    }

    if (!Utils.isEmpty(request.stockCode)) {
      filter.stockCode = request.stockCode;
    }

    if (!Utils.isEmpty(request.sellBuyType)) {
      filter.sbType = request.sellBuyType;
    }

    if (!Utils.isEmpty(request.orderType)) {
      filter.orderType = request.orderType;
    }

    if (!Utils.isEmpty(request.status)) {
      filter.status = request.status;
    }

    if (!Utils.isEmpty(request.bankCode)) {
      filter.bankCode = request.bankCode;
    }

    if (!Utils.isEmpty(request.bankAccount)) {
      filter.bankAccount = request.bankAccount;
    }

    if (request.fetchCount == null) {
      request.fetchCount = DEFAULT_PAGE_SIZE;
    }

    const filter1: FindConditions<StopOrder> = { ...filter };
    filter1.fromDate = Between(fromDate, toDate);
    const filter2: FindConditions<StopOrder> = { ...filter };
    filter2.toDate = Between(fromDate, toDate);
    const filter3: FindConditions<StopOrder> = { ...filter };
    filter3.fromDate = LessThanOrEqual(fromDate);
    filter3.toDate = MoreThanOrEqual(toDate);

    const stopOrders: StopOrder[] = await this.stopOrderRepository.queryStopOrderHistory(
      [filter1, filter2, filter3],
      request.fetchCount
    );
    if (stopOrders == null) {
      return [];
    } else {
      return stopOrders.map(toStopOrderHistoryResponse);
    }
  }

  public async cancelAllStopOrderBySchedule() {
    Logger.info('Cancel All stopOrder by Schedule');
    await getManager().transaction(
      async (transactionalEntityManager: EntityManager) => {
        const currentMarketDate: Date = new Date();
        currentMarketDate.setHours(
          currentMarketDate.getHours() + MARKET_TIMEZONE
        );

        await transactionalEntityManager.update(
          StopOrder,
          {
            status: STOP_ORDER_STATUS.PENDING,
            toDate: LessThanOrEqual(Utils.getEndOfDate(currentMarketDate)),
          },
          {
            status: STOP_ORDER_STATUS.CANCELLED,
            cancelledAt: new Date(),
          }
        );
      }
    );
    // sync with market-monitor
    await Kafka.getInstance().sendRequestAsync(
      uuid(),
      config.topic.marketMonitor,
      '/order/stop/cancelAllBySchedule',
      ''
    );
    return true;
  }

  public async cancelAllStopOrderByReq(
    request: IStopOrderCancelAllRequest
  ): Promise<any> {
    const invalidParams = new Errors.InvalidParameterError();
    Utils.validate(request.accountNumber, 'accountNumber')
      .setRequire()
      .throwValid(invalidParams);
    Utils.validate(request.subNumber, 'subNumber')
      .setRequire()
      .throwValid(invalidParams);
    Utils.validate(request.headers.token, 'token')
      .setRequire()
      .throwValid(invalidParams);

    invalidParams.throwErr();

    const criteria: any = {
      accountNumber: request.accountNumber,
      subNumber: request.subNumber,
    };

    if (request.bankCode != null) {
      criteria.bankCode = request.bankCode;
    }
    if (request.bankAccount != null) {
      criteria.bankAccount = request.bankAccount;
    }
    if (request.orderType != null) {
      criteria.orderType = request.orderType;
    }
    if (request.sellBuyType != null) {
      criteria.sbType = request.sellBuyType;
    }
    if (request.stockCode != null) {
      criteria.stockCode = request.stockCode;
    }
    if (request.stopPrice != null) {
      criteria.stopPrice = request.stopPrice;
    }

    const stopOrderList: StopOrder[] = await this.stopOrderRepository.findBy(
      criteria,
      Number.MAX_SAFE_INTEGER
    );
    const stopOrderIdList = [];
    stopOrderList.forEach((stopOrder: StopOrder) =>
      stopOrderIdList.push(stopOrder.id)
    );

    await getManager().transaction(
      async (transactionalEntityManager: EntityManager) => {
        await transactionalEntityManager.update(StopOrder, criteria, {
          status: STOP_ORDER_STATUS.CANCELLED,
          cancelledAt: new Date(),
        });
      }
    );

    // sync with market-monitor
    if (stopOrderIdList.length > 0) {
      await Kafka.getInstance().sendRequestAsync(
        uuid(),
        config.topic.marketMonitor,
        '/order/stop/cancel',
        stopOrderIdList
      );
    }
    return {};
  }

  public async queryTodayPendingStopOrder(
    request: IStopOrderActiveRequest
  ): Promise<StopOrder[]> {
    const currentMarketDate: Date = new Date();
    currentMarketDate.setHours(
      currentMarketDate.getHours() + config.market_time_zone
    );
    const fetchCount: number =
      request.fetchCount == null ? 100 : request.fetchCount;
    const lastStopOrderId: number =
      request.lastStopOrderId == null
        ? Number.MAX_SAFE_INTEGER
        : request.lastStopOrderId;
    const filter: FindConditions<StopOrder> = {
      status: STOP_ORDER_STATUS.PENDING,
      fromDate: LessThanOrEqual(Utils.getEndOfDate(currentMarketDate)),
      toDate: MoreThanOrEqual(Utils.getStartOfDate(currentMarketDate)),
      id: LessThan(lastStopOrderId),
    };
    return this.stopOrderRepository.findBy(filter, fetchCount, { id: 'DESC' });
  }

  public async updateStopOrder(stopOrder: StopOrder): Promise<object> {
    await getManager().transaction(
      async (transactionalEntityManager: EntityManager) => {
        await transactionalEntityManager.update(
          StopOrder,
          stopOrder.id,
          stopOrder
        );
      }
    );
    return {};
  }
}
