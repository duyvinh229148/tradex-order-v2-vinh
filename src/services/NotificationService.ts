import { Service } from 'typedi';
import { Logger, TradexNotification } from 'tradex-common';
import * as uuid from 'uuid';
import config from '../config';
import {
  OrderMatchResponse,
  parse,
} from '../models/notification/OrderMatchNotification';
import { AccountInfoRepository } from '../repositories/AccountInfoRepository';
import AccountInfo from '../models/db/AccountInfo';
import { InjectRepository } from 'typeorm-typedi-extensions';

@Service()
export default class NotificationService {
  @InjectRepository()
  private accountInfoRepository: AccountInfoRepository;

  private sendNotification: TradexNotification.SendNotification;

  constructor() {
    // do nothing
  }

  public getSendNotification() {
    if (this.sendNotification == null) {
      this.sendNotification = TradexNotification.getInstance();
    }
    return this.sendNotification;
  }

  public notifyOneSignal(
    data: TradexNotification.ITemplateData,
    filters: TradexNotification.IFilter[]
  ) {
    const filterByTagOtherNotifications: TradexNotification.IFilter = {
      field: 'tag',
      key: 'otherNotifications',
      relation: '!=',
      value: 'false',
    };

    // notify via one signal
    const conf: TradexNotification.OneSignalConfiguration = new TradexNotification.OneSignalConfiguration();
    conf.domain = config.domain;
    conf.filters = filters;
    conf.filters.push(filterByTagOtherNotifications);
    this.getSendNotification().sendPushNotification(uuid(), conf, data);
  }

  public async notifyMatchOrder(
    orderMatchResponse: OrderMatchResponse
  ): Promise<any> {
    try {
      const accountInfoList: AccountInfo[] = await this.accountInfoRepository.findByAccountNumberAndSubNumber(
        orderMatchResponse.accountNumber,
        orderMatchResponse.subNumber
      );
      if (accountInfoList == null || accountInfoList.length === 0) {
        return;
      }
      accountInfoList.forEach((accountInfo: AccountInfo) => {
        const filterByUsername: TradexNotification.IFilter = {
          field: 'tag',
          key: 'username',
          relation: '=',
          value: accountInfo.username,
        };
        this.notifyOneSignal(parse(orderMatchResponse), [filterByUsername]);
      });
    } catch (e) {
      Logger.error(`error while notifyMatchOrder: ${e}`);
    }
  }
}
