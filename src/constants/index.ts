export const DEFAULT_EQUITY_SUB_NUMBER = '00';
export const DEFAULT_DERIVATIVES_SUB_NUMBER = '80';
export const DEFAULT_FETCH_COUNT_ORDER_HISTORY = 100;
export const DEFAULT_DATE = '19700101';
export const DEFAULT_PAGE_SIZE = 20;
export const DEFAULT_SEQUENCE_DESC = Number.MAX_SAFE_INTEGER;
export const MARKET_TIMEZONE = 7;
export const DEFAULT_BASKET_ORDER_SIZE = 30;
export const DEFAULT_ORIGINAL_ORDER_NUMBER = '0';
export const CHUNK_SIZE = 50;

export const STOP_ORDER_STATUS = {
  PENDING: 'PENDING',
  COMPLETED: 'COMPLETED',
  CANCELLED: 'CANCELLED',
  FAILED: 'FAILED',
  SENDING: 'SENDING',
};

export enum ROUNDING_UNIT {
  HOSE = 10,
  UPCOM = 100,
  HNX = 100,
}

export const KBSV_DOMAIN = 'kbsv';

export const SECURITIES_TYPE = {
  INDEX: 'INDEX',
  STOCK: 'STOCK',
  FUTURES: 'FUTURES',
  CW: 'CW',
};

export enum BosOrderStatusEnum {
  RECEIPT = 'RECEIPT',
  SEND = 'SEND',
  ORDER_CONFIRM = 'ORDER_CONFIRM',
  RECEIPT_CONFIRM = 'RECEIPT_CONFIRM',
  FULL_FILLED = 'FULL_FILLED',
  PARTIAL_FILLED = 'PARTIAL_FILLED',
  REJECT = 'REJECT',
}

export enum OrderTypeEnum {
  LO = 'LO',
  MP = 'MP',
  ATO = 'ATO',
  ATC = 'ATC',
  MOK = 'MOK',
  MAK = 'MAK',
  MTL = 'MTL',
  PLO = 'PLO',
}

export enum StopOrderTypeEnum {
  STOP = 'STOP',
  STOP_LIMIT = 'STOP_LIMIT',
}

export enum SellBuyTypeEnum {
  SELL = 'SELL',
  BUY = 'BUY',
}

export enum SortTypeEnum {
  DESC = 'DESC',
  ASC = 'ASC',
}

export enum MarketTypeEnum {
  HNX = 'HNX',
  HOSE = 'HOSE',
  UPCOM = 'UPCOM',
}

export const URI = {
  PLACE_NORMAL_ORDER: '/api/v1/equity/order',
  MODIFY_NORMAL_ORDER: '/api/v1/equity/order/modify',
  CANCEL_NORMAL_ORDER: '/api/v1/equity/order/cancel',
  TUXEDO_ORDER_HISTORY: '/api/v1/equity/order/history',
  FSS_PLACE_NORMAL_ORDER: '/api/v1/equity/account/stopOrder/active',
  FSS_AUTHEN_ORDER: '/api/v1/equity/verifyOrder',
  QUERY_ORDER_HISTORY: '/api/v1/equity/order/history',
  QUERY_ACCOUNT_PROFIT_LOSS: '/api/v1/equity/account/profitLoss',
  QUERY_ACCOUNT_STOCK_BALANCE_HISTORY:
    '/api/v1/equity/account/stockBalance/history',
  QUERY_STOCK_DAILY: '/api/v1/market/stock/{stockCode}/period/{periodType}',
  STOP_ORDER_ACTIVATION_COMPLETED: '/api/v1/notify/activeStopOrderCompleted',
  STOP_ORDER_ACTIVATION_FAILED: '/api/v1/notify/activeStopOrderFailed',
};

export enum BasketOrderPriceTypeEnum {
  BEST_ASK_1 = 'BEST_ASK_1',
  BEST_ASK_2 = 'BEST_ASK_2',
  BEST_BID_1 = 'BEST_BID_1',
  BEST_BID_2 = 'BEST_BID_2',
  ATO = 'ATO',
  ATC = 'ATC',
  LO = 'LO',
  MARKET_PRICE = 'MARKET_PRICE',
}

export enum RoundingEnum {
  UP = 'UP',
  DOWN = 'DOWN',
}

export enum BasketStatusEnum {
  PENDING = 'PENDING',
  COMPLETED = 'COMPLETED',
  CANCELLED = 'CANCELLED',
  DRAFT = 'DRAFT',
  DEFAULT = 'DEFAULT',
}

export enum OrderStatusEnum {
  SENDING = 'SENDING',
  PENDING = 'PENDING',
  COMPLETED = 'COMPLETED',
  CANCELLED = 'CANCELLED',
  FAILED = 'FAILED',
}

export enum BasketSecuritiesTypeEnum {
  STOCK = 'STOCK',
  FUND = 'FUND',
  ETF = 'ETF',
  BOND = 'BOND',
  FUTURES = 'FUTURES',
  CW = 'CW',
}

export enum ModifyCancelTypeEnum {
  NORMAL = 'NORMAL',
  PARTIAL_CORRECTION = 'PARTIAL_CORRECTION',
  ALL_CORRECTION = 'ALL_CORRECTION',
  PARTIAL_CANCEL = 'PARTIAL_CANCEL',
  ALL_CANCEL = 'ALL_CANCEL',
}
