import config from '../config';
import OrderHistoryResponse from '../models/response/OrderHistoryResponse';
import { SellBuyTypeEnum } from '../constants';
import { Container } from 'typedi';
import DailyProfitService from '../services/DailyProfitService';
import { Kafka, Logger } from 'tradex-common';
import connection from '../utils/dbConnection';
import { Connection } from 'typeorm';
import { ISymbolInfo } from '../models/ISymbolInfo';

Logger.create(config.logger.config, true);
Logger.info('Starting...');

connection
  .then(async (connection: Connection) => {
    Kafka.create(
      config,
      config.kafkaOptions,
      true,
      { 'auto.offset.reset': 'earliest' },
      config.kafkaOptions
    );

    test();
  })
  .catch((error: any) => Logger.error(error));

const test = () => {
  const orderHistoryResponseList: OrderHistoryResponse[] = [];
  const oh1: OrderHistoryResponse = new OrderHistoryResponse();
  oh1.stockCode = 'AAA';
  oh1.matchedQuantity = 5;
  oh1.matchedPrice = 10000;
  oh1.sellBuyType = SellBuyTypeEnum.SELL.valueOf();

  const oh2: OrderHistoryResponse = new OrderHistoryResponse();
  oh2.stockCode = 'AAA';
  oh2.matchedQuantity = 5;
  oh2.matchedPrice = 9000;
  oh2.sellBuyType = SellBuyTypeEnum.SELL.valueOf();

  const oh3: OrderHistoryResponse = new OrderHistoryResponse();
  oh3.stockCode = 'AAA';
  oh3.matchedQuantity = 5;
  oh3.matchedPrice = 8000;
  oh3.sellBuyType = SellBuyTypeEnum.SELL.valueOf();

  const oh4: OrderHistoryResponse = new OrderHistoryResponse();
  oh4.stockCode = 'AAA';
  oh4.matchedQuantity = 7;
  oh4.matchedPrice = 9000;
  oh4.sellBuyType = SellBuyTypeEnum.BUY.valueOf();

  const oh5: OrderHistoryResponse = new OrderHistoryResponse();
  oh5.stockCode = 'AAA';
  oh5.matchedQuantity = 2;
  oh5.matchedPrice = 10000;
  oh5.sellBuyType = SellBuyTypeEnum.BUY.valueOf();

  const oh6: OrderHistoryResponse = new OrderHistoryResponse();
  oh6.stockCode = 'AAA';
  oh6.matchedQuantity = 5;
  oh6.matchedPrice = 8000;
  oh6.sellBuyType = SellBuyTypeEnum.SELL.valueOf();

  orderHistoryResponseList.push(oh1);
  orderHistoryResponseList.push(oh2);
  orderHistoryResponseList.push(oh3);
  orderHistoryResponseList.push(oh4);
  orderHistoryResponseList.push(oh5);
  orderHistoryResponseList.push(oh6);

  const stockBalance: number = 9;
  const stockInfo: ISymbolInfo = {};
  stockInfo.last = 8000;
  stockInfo.referencePrice = 9000;

  const dailyProfitService = Container.get(DailyProfitService);
  const dailyProfit = dailyProfitService.calculateDailyProfit(
    stockInfo,
    stockBalance,
    orderHistoryResponseList,
    new Date()
  );
  console.log(`dailyProfit: ${dailyProfit}`);
};

// How to calculate daily profit
// - Scan order history for each day
//
// 1 - If Buy Today and Sell Today -> Profit = (Sell Price - Buy Price) * Sell Qty
// 2 - If Buy not today and Sell Today -> Profit = (Sell Price - Yesterday Close Price) * Sell Qty
// 3 - If Buy not today and not sell -> Profit = (Today Close Price - Yesterday Close Price) * Current Qty
// 4 - If Buy today and not sell -> Profit = (Today Close Price - Buy Price) * Buy Qty
//
// Step 1: quét Sell
// Step 2: nếu Sell trước Buy thì tính luôn 2 còn ko tính 1
// Step 3: nếu còn dư Sell thì tính 2
// Step 4: quét Buy nếu còn dư Buy từ Step 2 và Step 3 -> tính 4
// Step 5: query stock balance -> nếu còn dư stock balance ngoài khoảng (Buy - Sell) vừa tính ở Step 2, Step 3, Step 4 -> tính 3
