import { Inject, Service } from 'typedi';
import { Subject } from 'rx';
import config from '../config';
import { Kafka, Utils, Errors, ServiceRegistration } from 'tradex-common';
import StopOrderService from '../services/StopOrderService';
import DailyProfitService from '../services/DailyProfitService';
import { KBSV_DOMAIN } from '../constants';
import FssStopOrderService from '../services/FssStopOrderService';
import NotificationService from '../services/NotificationService';

@Service()
export default class RequestHandler {
  @Inject()
  private stopOrderService: StopOrderService;
  @Inject()
  private fssStopOrderService: FssStopOrderService;
  @Inject()
  private dailyProfitService: DailyProfitService;
  @Inject()
  private notificationService: NotificationService;

  public init() {
    ServiceRegistration.create(Kafka.getInstance(), {
      nodeId: config.nodeId,
      serviceName: config.clusterId,
    });

    const handle: Kafka.MessageHandler = new Kafka.MessageHandler();
    new Kafka.StreamHandler(
      config,
      config.kafkaOptions,
      [config.clusterId],
      (message: any) => handle.handle(message, this.handleRequest)
    );
  }

  private handleRequest: Kafka.Handle = (message: Kafka.IMessage) => {
    if (message == null || message.data == null) {
      const subject: Subject<any> = new Subject();
      Utils.onError(subject, new Errors.SystemError());
      return false;
    } else {
      if (config.domain === KBSV_DOMAIN) {
        if (message.uri === '/api/v1/equity/order/stop') {
          return this.fssStopOrderService.placeStopOrder(message.data);
        } else if (message.uri === '/api/v1/equity/order/stop/modify') {
          return this.fssStopOrderService.modifyStopOrder(message.data);
        } else if (message.uri === '/api/v1/equity/order/stop/cancel') {
          return this.fssStopOrderService.cancelStopOrder(message.data);
        } else if (message.uri === '/api/v1/equity/order/stop/cancel/multi') {
          return this.fssStopOrderService.cancelMultiStopOrder(message.data);
        } else if (message.uri === '/api/v1/equity/order/stop/history') {
          return this.fssStopOrderService.queryStopOrderHistory(message.data);
        } else if (message.uri === '/api/v1/equity/order/stop/cancel/all') {
          return this.fssStopOrderService.cancelAllStopOrderByReq(message.data);
        }
        // using for syn with market-monitor
        else if (message.uri === '/order/stop/all') {
          return this.fssStopOrderService.queryTodayPendingStopOrder(
            message.data
          );
        } else if (message.uri === '/order/stop/update') {
          return this.fssStopOrderService.updateStopOrder(message.data);
        }
      } else {
        if (message.uri === '/api/v1/equity/order/stop') {
          return this.stopOrderService.placeStopOrder(message.data);
        } else if (message.uri === '/api/v1/equity/order/stop/modify') {
          return this.stopOrderService.modifyStopOrder(message.data);
        } else if (message.uri === '/api/v1/equity/order/stop/cancel') {
          return this.stopOrderService.cancelStopOrder(message.data);
        } else if (message.uri === '/api/v1/equity/order/stop/cancel/multi') {
          return this.stopOrderService.cancelMultiStopOrder(message.data);
        } else if (message.uri === '/api/v1/equity/order/stop/history') {
          return this.stopOrderService.queryStopOrderHistory(message.data);
        } else if (message.uri === '/api/v1/equity/order/stop/cancel/all') {
          return this.stopOrderService.cancelAllStopOrderByReq(message.data);
        } else if (message.uri === '/api/v1/equity/account/dailyProfit') {
          return this.dailyProfitService.queryDailyProfit(message.data);
        } else if (message.uri === '/api/v1/match_order') {
          return this.notificationService.notifyMatchOrder(message.data);
        }

        // using for syn with market-monitor
        else if (message.uri === '/order/stop/all') {
          return this.stopOrderService.queryTodayPendingStopOrder(message.data);
        } else if (message.uri === '/order/stop/update') {
          return this.stopOrderService.updateStopOrder(message.data);
        }
      }
    }

    return false;
  };
}
