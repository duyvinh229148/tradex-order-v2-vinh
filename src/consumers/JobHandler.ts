import { Inject, Service } from 'typedi';
import { Subject } from 'rx';
import config from '../config';
import { Errors, Kafka, Logger, Utils } from 'tradex-common';
import StopOrderService from '../services/StopOrderService';
import DailyProfitService from '../services/DailyProfitService';
import FssStopOrderService from '../services/FssStopOrderService';
import { KBSV_DOMAIN } from '../constants';

@Service()
export default class JobHandler {
  @Inject()
  private stopOrderService: StopOrderService;
  @Inject()
  private fssStopOrderService: FssStopOrderService;
  @Inject()
  private dailyProfitService: DailyProfitService;

  public init() {
    const handle: Kafka.MessageHandler = new Kafka.MessageHandler();
    new Kafka.StreamHandler(
      config,
      config.kafkaOptions,
      [config.topic.orderJob],
      (message: any) => handle.handle(message, this.handleRequest)
    );
  }

  private handleRequest: Kafka.Handle = (message: Kafka.IMessage) => {
    if (message == null) {
      const subject: Subject<any> = new Subject();
      Utils.onError(subject, new Errors.SystemError());
      return false;
    } else {
      if (message.uri === '/job/cancelAllStopOrder') {
        if (config.domain === KBSV_DOMAIN) {
          this.fssStopOrderService
            .cancelAllStopOrderBySchedule()
            .then()
            .catch((err: any) => Logger.error(`${err}`));
        } else {
          this.stopOrderService
            .cancelAllStopOrderBySchedule()
            .then()
            .catch((err: any) => Logger.error(`${err}`));
        }
        return true;
      } else if (message.uri === '/job/dailyProfit') {
        this.dailyProfitService
          .calculateDailyProfitByJob()
          .then()
          .catch((err: any) => Logger.error(`${err}`));
        return true;
      }
    }
    return false;
  };
}
