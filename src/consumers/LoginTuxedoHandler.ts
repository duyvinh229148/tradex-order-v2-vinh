import { Inject, Service } from 'typedi';
import config from '../config';
import { Kafka, Logger } from 'tradex-common';
import AccountInfoService from '../services/AccountInfoService';
import { LoginTuxedoResponse } from '../models/response/LoginTuxedoResponse';

@Service()
export default class LoginTuxedoHandler {
  @Inject()
  private accountInfoService: AccountInfoService;

  public init() {
    new Kafka.StreamHandler(
      config,
      config.kafkaOptions,
      [config.topic.loginTuxedoNotify],
      (message: any) => this.handleRequest(message)
    );
  }

  private handleRequest = (data: any) => {
    try {
      const msgString: string = data.value.toString();
      Logger.info(`Got message ${msgString}`);
      const message: Kafka.IMessage = JSON.parse(msgString);

      if (message != null && message.data != null) {
        if (message.uri === '/notify') {
          const loginTuxedoResponse: LoginTuxedoResponse = message.data;
          this.accountInfoService
            .saveOrUpdateAccountInfo(loginTuxedoResponse)
            .then()
            .catch((err: any) => Logger.error(`${err}`));
        }
      }
    } catch (e) {
      Logger.logError('error while processing message', e);
    }
  };
}
