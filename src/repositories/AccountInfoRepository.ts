import { EntityRepository, Repository } from 'typeorm';
import { Service } from 'typedi';
import AccountInfo from '../models/db/AccountInfo';

@Service()
@EntityRepository(AccountInfo)
export class AccountInfoRepository extends Repository<AccountInfo> {
  public findOneByAccountNumberAndSubNumber(
    accountNumber: string,
    subNumber: string
  ): Promise<AccountInfo> {
    return this.findOne({
      accountNumber,
      subNumber,
    });
  }

  public findByAccountNumberAndSubNumber(
    accountNumber: string,
    subNumber: string
  ): Promise<AccountInfo[]> {
    return this.find({
      accountNumber: accountNumber,
      subNumber: subNumber,
    });
  }

  public findAll(): Promise<AccountInfo[]> {
    return this.find({});
  }
}
