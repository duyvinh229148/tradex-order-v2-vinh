import { EntityRepository, Repository, FindConditions } from 'typeorm';
import { Service } from 'typedi';
import StopOrder from '../models/db/StopOrder';
import { DEFAULT_PAGE_SIZE } from '../constants';

@Service()
@EntityRepository(StopOrder)
export class StopOrderRepository extends Repository<StopOrder> {
  public findOneBy(conditions: FindConditions<StopOrder>): Promise<StopOrder> {
    return this.findOne(conditions);
  }

  public findBy(
    filter: any,
    fetchCount: number = DEFAULT_PAGE_SIZE,
    sort: any = { createdAt: 'DESC' }
  ): Promise<StopOrder[]> {
    return this.find({
      order: sort,
      where: filter,
      take: fetchCount,
    });
  }

  public queryStopOrderHistory(
    filter: FindConditions<StopOrder>[],
    limit: number = 20
  ): Promise<StopOrder[]> {
    return this.find({
      order: {
        id: 'DESC',
      },
      where: filter,
      take: limit,
    });
  }
}
