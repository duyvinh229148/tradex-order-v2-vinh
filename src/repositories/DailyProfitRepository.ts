import { EntityRepository, Repository } from 'typeorm';
import { Service } from 'typedi';
import DailyProfit from '../models/db/DailyProfit';

@Service()
@EntityRepository(DailyProfit)
export class DailyProfitRepository extends Repository<DailyProfit> {
  public findLastByAccountNumberAndSubNumber(
    accountNumber: string,
    subNumber: string
  ): Promise<DailyProfit> {
    return this.findOne(
      {
        accountNumber,
        subNumber,
      },
      {
        order: {
          date: 'DESC',
        },
      }
    );
  }
}
