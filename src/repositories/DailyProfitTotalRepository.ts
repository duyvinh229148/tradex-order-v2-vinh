import { EntityRepository, Repository } from 'typeorm';
import { Service } from 'typedi';
import DailyProfitTotal from '../models/db/DailyProfitTotal';
import { DEFAULT_PAGE_SIZE } from '../constants';

@Service()
@EntityRepository(DailyProfitTotal)
export class DailyProfitTotalRepository extends Repository<DailyProfitTotal> {
  public findLastByAccountNumberAndSubNumber(
    accountNumber: string,
    subNumber: string
  ): Promise<DailyProfitTotal> {
    return this.findOne(
      {
        accountNumber,
        subNumber,
      },
      {
        order: {
          date: 'DESC',
        },
      }
    );
  }

  public findOneBy(filter: any): Promise<DailyProfitTotal> {
    return this.findOne({ where: filter });
  }

  public findBy(
    filter: any,
    limit: number = DEFAULT_PAGE_SIZE,
    sort: any = { date: 'DESC' }
  ): Promise<DailyProfitTotal[]> {
    return this.find({
      order: sort,
      where: filter,
      take: limit,
    });
  }
}
