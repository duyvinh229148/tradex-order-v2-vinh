import {
  StopOrderPlaceResponse,
  StopOrderModifyResponse,
  StopOrderHistoryResponse,
} from 'tradex-models-order';
import StopOrder from '../models/db/StopOrder';
import { Utils } from 'tradex-common';
import {
  SellBuyTypeEnum,
  STOP_ORDER_STATUS,
  StopOrderTypeEnum,
} from '../constants';
import AccountInfo from '../models/db/AccountInfo';
import {
  Account,
  AccountSub,
  LoginTuxedoResponse,
} from '../models/response/LoginTuxedoResponse';
import { ILoginFssResponse } from '../models/response/LoginFssResponse';

const toStopOrderPlaceResponse = (
  stopOrder: StopOrder
): StopOrderPlaceResponse => {
  return {
    id: stopOrder.id,
  };
};

const toStopOrderModifyResponse = (): StopOrderModifyResponse => {
  return {};
};

const toStopOrderHistoryResponse = (
  stopOrder: StopOrder
): StopOrderHistoryResponse => {
  const response: StopOrderHistoryResponse = {};
  if (stopOrder != null) {
    response.sequence = stopOrder.id;
    response.stockCode = stopOrder.stockCode;
    response.bankCode = stopOrder.bankCode;
    response.bankAccount = stopOrder.bankAccount;
    response.orderQuantity = stopOrder.quantity;
    response.sellBuyType = SellBuyTypeEnum[stopOrder.sbType];
    response.stopPrice = Utils.round(stopOrder.stopPrice);
    response.orderPrice = Utils.round(stopOrder.orderPrice);
    response.orderType = StopOrderTypeEnum[stopOrder.orderType];
    response.orderNumber = stopOrder.orderNumber;
    response.status = STOP_ORDER_STATUS[stopOrder.status];
    response.createTime = Utils.formatDateToDisplay(
      stopOrder.createdAt,
      Utils.DATETIME_DISPLAY_FORMAT
    );
    response.orderTime = Utils.formatDateToDisplay(
      stopOrder.orderedAt,
      Utils.DATETIME_DISPLAY_FORMAT
    );
    response.cancelTime = Utils.formatDateToDisplay(
      stopOrder.cancelledAt,
      Utils.DATETIME_DISPLAY_FORMAT
    );
    response.fromDate = Utils.formatDateToDisplay(
      stopOrder.fromDate,
      Utils.DATE_DISPLAY_FORMAT
    );
    response.toDate = Utils.formatDateToDisplay(
      stopOrder.toDate,
      Utils.DATE_DISPLAY_FORMAT
    );
    if (!Utils.isEmpty(stopOrder.failReason)) {
      response.errorMessage = JSON.parse(stopOrder.failReason).code;
    }
  }

  return response;
};

const toAccountInfoList = (
  loginTuxedoResponse: LoginTuxedoResponse
): AccountInfo[] => {
  const accountInfoList: AccountInfo[] = [];
  const accounts: Account[] = loginTuxedoResponse.userInfo.accounts;
  for (let i = 0; i < accounts.length; i++) {
    const account: Account = accounts[i];
    for (let j = 0; j < account.accountSubs.length; j++) {
      const accountSub: AccountSub = account.accountSubs[j];
      const accountInfo = new AccountInfo();
      accountInfo.username = loginTuxedoResponse.userInfo.username.toLowerCase();
      accountInfo.accountNumber = account.accountNumber;
      accountInfo.subNumber = accountSub.subNumber;
      if (
        accountSub.bankAccounts != null &&
        accountSub.bankAccounts.length > 0
      ) {
        accountInfo.bankCode = accountSub.bankAccounts[0].bankCode;
        accountInfo.bankName = accountSub.bankAccounts[0].bankName;
      }
      accountInfo.identifierNumber =
        loginTuxedoResponse.userData.identifierNumber;
      accountInfo.branchCode = loginTuxedoResponse.userData.branchCode;
      accountInfo.mngDeptCode = loginTuxedoResponse.userData.mngDeptCode;
      accountInfo.deptCode = loginTuxedoResponse.userData.deptCode;
      accountInfo.agencyNumber = loginTuxedoResponse.userData.agencyNumber;
      accountInfo.userType = loginTuxedoResponse.userData.userType;
      accountInfoList.push(accountInfo);
    }
  }
  return accountInfoList;
};

const toFssAccountInfoList = (
  loginFssResponse: ILoginFssResponse
): AccountInfo[] => {
  const accountInfoList: AccountInfo[] = [];
  const accounts: Account[] = loginFssResponse.userInfo.accounts;
  for (let i = 0; i < accounts.length; i++) {
    const account: Account = accounts[i];
    for (let j = 0; j < account.accountSubs.length; j++) {
      const accountSub: AccountSub = account.accountSubs[j];
      const accountInfo = new AccountInfo();
      accountInfo.username = loginFssResponse.userInfo.username;
      accountInfo.accountNumber = account.accountNumber;
      accountInfo.subNumber = accountSub.subNumber;
      if (
        accountSub.bankAccounts != null &&
        accountSub.bankAccounts.length > 0
      ) {
        accountInfo.bankCode = accountSub.bankAccounts[0].bankCode;
        accountInfo.bankName = accountSub.bankAccounts[0].bankName;
      }
      accountInfo.identifierNumber = loginFssResponse.userData.identifierNumber;
      accountInfo.branchCode = loginFssResponse.userData.branchCode;
      accountInfo.mngDeptCode = loginFssResponse.userData.mngDeptCode;
      accountInfo.deptCode = loginFssResponse.userData.deptCode;
      accountInfo.agencyNumber = loginFssResponse.userData.agencyNumber;
      accountInfo.userType = loginFssResponse.userInfo.userType;
      accountInfoList.push(accountInfo);
    }
  }
  return accountInfoList;
};

export {
  toStopOrderPlaceResponse,
  toStopOrderModifyResponse,
  toStopOrderHistoryResponse,
  toAccountInfoList,
  toFssAccountInfoList,
};
