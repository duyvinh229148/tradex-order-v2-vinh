import {
  StopOrderPlaceRequest,
  FssStopOrderPlaceRequest,
} from 'tradex-models-order';
import StopOrder from '../models/db/StopOrder';
import { DEFAULT_EQUITY_SUB_NUMBER } from '../constants';

const fromStopOrderPlaceRequest = (
  request: StopOrderPlaceRequest,
  fromDate: Date,
  toDate: Date
): StopOrder => {
  const stopOrder = new StopOrder();
  stopOrder.stockCode = request.stockCode;
  stopOrder.userId = request.headers.token.userId;
  stopOrder.connectionId = request.headers.token.connectionId;
  stopOrder.serviceUsername = request.headers.token.serviceUsername;
  stopOrder.createdBy = request.headers.token.userId;
  stopOrder.quantity = request.orderQuantity;
  stopOrder.sbType = request.sellBuyType;
  stopOrder.stopPrice = request.stopPrice;
  stopOrder.orderPrice = request.orderPrice;
  stopOrder.serviceCode = request.headers.token.serviceCode;
  stopOrder.username = request.headers.token.userData.username;
  stopOrder.accountNumber = request.accountNumber;
  stopOrder.subNumber =
    request.subNumber == null ? DEFAULT_EQUITY_SUB_NUMBER : request.subNumber;
  stopOrder.bankCode = request.bankCode;
  stopOrder.bankAccount = request.bankAccount;
  stopOrder.securitiesType = request.securitiesType;
  stopOrder.orderType = request.orderType;
  stopOrder.token = JSON.stringify(request.headers.token);
  stopOrder.sourceIp = request.sourceIp;
  stopOrder.fromDate = fromDate;
  stopOrder.toDate = toDate;
  return stopOrder;
};

const fromFssStopOrderPlaceRequest = (
  request: FssStopOrderPlaceRequest,
  fromDate: Date,
  toDate: Date
): StopOrder => {
  const stopOrder = new StopOrder();
  stopOrder.stockCode = request.stockCode;
  stopOrder.userId = request.headers.token.userId;
  stopOrder.connectionId = request.headers.token.connectionId;
  stopOrder.serviceUsername = request.headers.token.serviceUsername;
  stopOrder.createdBy = request.headers.token.userId;
  stopOrder.quantity = request.orderQuantity;
  stopOrder.sbType = request.sellBuyType;
  stopOrder.stopPrice = request.stopPrice;
  stopOrder.orderPrice = request.orderPrice;
  stopOrder.serviceCode = request.headers.token.serviceCode;
  stopOrder.username = request.headers.token.userData.username;
  stopOrder.accountNumber = request.accountNumber;
  stopOrder.securitiesType = request.securitiesType;
  stopOrder.orderType = request.orderType;
  stopOrder.token = JSON.stringify(request.headers.token);
  stopOrder.sourceIp = request.sourceIp;
  stopOrder.fromDate = fromDate;
  stopOrder.toDate = toDate;

  // only for kb
  stopOrder.requestId = request.requestId;
  stopOrder.otpCode = request.otpCode;
  stopOrder.transactionID = request.transactionID;
  stopOrder.channelName = request.channelName;
  stopOrder.tokenId = request.tokenId;
  stopOrder.pinCode = request.pinCode;
  stopOrder.thumbPrint = request.thumbPrint;
  stopOrder.serialNumber = request.serialNumber;
  stopOrder.remember2Factor = request.remember2Factor;

  return stopOrder;
};

export { fromStopOrderPlaceRequest, fromFssStopOrderPlaceRequest };
