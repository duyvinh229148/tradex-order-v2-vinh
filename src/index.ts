import config from './config';
import 'reflect-metadata';
import connection from './utils/dbConnection';
import { Connection } from 'typeorm';
import { Kafka, Logger, TradexNotification } from 'tradex-common';
import { Container } from 'typedi';
import RequestHandler from './consumers/RequestHandler';
import JobHandler from './consumers/JobHandler';
import LoginTuxedoHandler from './consumers/LoginTuxedoHandler';
import LoginFssHandler from './consumers/LoginFssHandler';
import RedisDao from './daos/RedisDao';

Logger.create(config.logger.config, true);
Logger.info('Starting...');
connection
  .then(async (connection: Connection) => {
    Kafka.create(
      config,
      config.kafkaOptions,
      true,
      { 'auto.offset.reset': 'earliest' },
      config.kafkaOptions
    );

    TradexNotification.create(Kafka.getInstance());

    const redisDao = Container.get(RedisDao);
    await redisDao.init();
    Logger.info('connected to redis!');

    const loginTuxedoHandler = Container.get(LoginTuxedoHandler);
    loginTuxedoHandler.init();

    const loginFssHandler = Container.get(LoginFssHandler);
    loginFssHandler.init();

    const requestHandler = Container.get(RequestHandler);
    requestHandler.init();

    const jobHandler = Container.get(JobHandler);
    jobHandler.init();
  })
  .catch((error: any) => Logger.error(error));
