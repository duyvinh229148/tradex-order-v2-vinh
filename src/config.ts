/* tslint:disable */
import { v4 as uuid } from 'uuid';
import { Utils } from 'tradex-common';

const nodeId = uuid();
let config = {
  db: {
    client: 'mysql',
    connection: {
      host: Utils.getEnvStr('TRADEX_ENV_MYSQL_HOST'),
      user: Utils.getEnvStr('TRADEX_ENV_MYSQL_USER'),
      password: Utils.getEnvStr('TRADEX_ENV_MYSQL_PASSWORD'),
      database: 'tradex-order',
    },
  },
  redis: {
    port: 6379,
    host: '172.31.43.101',
    options: {
      password: 'aO-6099lU8Y_-RsLuN8DX_y4KSp_8GJx_-WK-kB_G50trxkI1C_wvGQS-H1x',
    },
  },
  logger: {
    config: {
      appenders: {
        application: { type: 'console' },
        file: {
          type: 'file',
          filename: '/logs/application.log',
          compression: true,
          maxLogSize: 10485760,
          backups: 10,
        },
      },
      categories: {
        default: { appenders: ['application', 'file'], level: 'info' },
      },
    },
  },
  log: {
    serviceName: 'order-service',
    format: 'FLAT', // 'FLAT' or 'JSON'
    transport: [],
  },
  basketOrderDefaultList: ['E1VFVN30', 'FUESSV50'],
  topic: {
    bidOfferUpdate: 'bidOfferUpdate',
    cwBidOfferUpdate: 'cwBidOfferUpdate',
    stockUpdate: 'stockUpdate',
    marketStatus: 'marketStatus',
    cwUpdate: 'cwUpdate',
    futuresUpdate: 'futuresUpdate',
    configuration: 'configuration',
    loginTuxedoNotify: 'loginTuxedoNotify',
    tuxedo: 'tuxedo',
    fss: 'fss-rest-bridge',
    orderJob: 'orderJob',
    market: 'market',
    marketMonitor: 'market-monitor',
    user: 'user',
    loginFssNotify: 'loginFssNotify',
  },
  holidays: ['20200402', '20200430', '20200501', '20200902'],
  kafka_timeout: 10000,
  clusterId: 'order',
  clientId: `order-${nodeId}`,
  nodeId: nodeId,
  time_cancel_all_stop_order: {
    hour: 15,
    minute: 30,
  },
  market_time_zone: 7,
  domain: Utils.getEnvStr('TRADEX_ENV_DOMAIN'),
  kafkaUrls: Utils.getEnvArr('TRADEX_ENV_KAFKA_URLS'),
  kafkaOptions: {},
};

try {
  const env = require('./env');
  if (env) {
    config = { ...config, ...env(config) };
  }
} catch (e) {
  //swalow it
}
export default config;
