import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
import { ISymbolInfo } from '../ISymbolInfo';

@Entity('t_daily_profit')
export default class DailyProfit {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({ name: 'account_number' })
  public accountNumber: string;

  @Column({ name: 'sub_number' })
  public subNumber: string;

  @Column({ name: 'stock_code' })
  public stockCode: string;

  @Column({ name: 'stock_balance' })
  public stockBalance: number;

  @Column({ name: 'date' })
  public date: Date;

  @Column({ name: 'profit' })
  public profit: number;

  @Column({ name: 'sell_amount' })
  public sellAmount: number;

  @Column({ name: 'buy_amount' })
  public buyAmount: number;

  @Column({ name: 'selling_profit' })
  public sellingProfit: number;

  @Column({ name: 'buying_profit' })
  public buyingProfit: number;

  @Column({ name: 'balance_profit' })
  public balanceProfit: number;

  @Column({ name: 'today_evaluated_amount' })
  public todayEvaluatedAmount: number;

  @Column({ name: 'yesterday_evaluated_amount' })
  public yesterdayEvaluatedAmount: number;

  @Column({ name: 'profit_ratio' })
  public profitRatio: number;

  @Column({ name: 'created_at' })
  public createdAt: Date;

  @Column({ name: 'updated_at' })
  public updatedAt: Date;

  @Column({ type: 'json', name: 'securities_info' })
  public securitiesInfo: ISymbolInfo;
}
