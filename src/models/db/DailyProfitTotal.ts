import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
import DailyProfit from './DailyProfit';
import { Logger } from 'tradex-common';

@Entity('t_daily_profit_total')
export default class DailyProfitTotal {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({ name: 'account_number' })
  public accountNumber: string;

  @Column({ name: 'sub_number' })
  public subNumber: string;

  @Column({ name: 'date' })
  public date: Date;

  @Column({ name: 'daily_profit' })
  public dailyProfit: number;

  @Column({ name: 'total_profit' })
  public totalProfit: number;

  @Column({ name: 'sell_amount' })
  public sellAmount: number;

  @Column({ name: 'buy_amount' })
  public buyAmount: number;

  @Column({ name: 'selling_profit' })
  public sellingProfit: number;

  @Column({ name: 'buying_profit' })
  public buyingProfit: number;

  @Column({ name: 'balance_profit' })
  public balanceProfit: number;

  @Column({ name: 'today_evaluated_amount' })
  public todayEvaluatedAmount: number;

  @Column({ name: 'yesterday_evaluated_amount' })
  public yesterdayEvaluatedAmount: number;

  @Column({ name: 'profit_ratio' })
  public profitRatio: number;

  @Column({ name: 'total_selling_amount' })
  public totalSellingAmount: number;

  @Column({ name: 'total_buying_amount' })
  public totalBuyingAmount: number;

  @Column({ name: 'total_selling_profit' })
  public totalSellingProfit: number;

  @Column({ name: 'total_buying_profit' })
  public totalBuyingProfit: number;

  @Column({ name: 'total_balance_profit' })
  public totalStockBalanceProfit: number;

  @Column({ name: 'created_at' })
  public createdAt: Date;

  @Column({ name: 'updated_at' })
  public updatedAt: Date;

  public updateByDailyProfitDetail(dailyProfit: DailyProfit) {
    this.dailyProfit += dailyProfit.profit;
    this.totalProfit += dailyProfit.profit;
    this.sellAmount += dailyProfit.sellAmount;
    this.sellingProfit += dailyProfit.sellingProfit;
    this.buyAmount += dailyProfit.buyAmount;
    this.buyingProfit += dailyProfit.buyingProfit;
    this.balanceProfit += dailyProfit.balanceProfit;
    this.yesterdayEvaluatedAmount += dailyProfit.yesterdayEvaluatedAmount;
    this.todayEvaluatedAmount += dailyProfit.todayEvaluatedAmount;
    this.totalSellingAmount += dailyProfit.sellAmount;
    this.totalBuyingAmount += dailyProfit.buyAmount;
    this.totalSellingProfit += dailyProfit.sellingProfit;
    this.totalBuyingProfit += dailyProfit.buyingProfit;
    this.totalStockBalanceProfit += dailyProfit.balanceProfit;
    this.profitRatio = 0;

    if (this.yesterdayEvaluatedAmount === 0 && this.balanceProfit === 0) {
      Logger.error(
        `data error: both yesterdayEvaluatedAmount and todayEvaluatedAmount = 0`
      );
    } else {
      this.profitRatio = this.totalProfit / this.yesterdayEvaluatedAmount;
    }
  }
}
