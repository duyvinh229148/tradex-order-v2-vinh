import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('t_account_info')
export default class AccountInfo {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({ name: 'username' })
  public username: string;

  @Column({ name: 'account_number' })
  public accountNumber: string;

  @Column({ name: 'sub_number' })
  public subNumber: string;

  @Column({ name: 'bank_code' })
  public bankCode: string;

  @Column({ name: 'bank_name' })
  public bankName: string;

  @Column({ name: 'identifier_number' })
  public identifierNumber: string;

  @Column({ name: 'branch_code' })
  public branchCode: string;

  @Column({ name: 'mng_dept_code' })
  public mngDeptCode: string;

  @Column({ name: 'dept_code' })
  public deptCode: string;

  @Column({ name: 'agency_number' })
  public agencyNumber: string;

  @Column({ name: 'user_type' })
  public userType: string;

  @Column({ name: 'created_at' })
  public createdAt: Date;

  @Column({ name: 'updated_at' })
  public updatedAt: Date;
}
