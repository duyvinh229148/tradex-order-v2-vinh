import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { STOP_ORDER_STATUS, StopOrderTypeEnum } from '../../constants';

@Entity('t_stop_order')
export default class StopOrder {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({ name: 'stock_code' })
  public stockCode: string;

  @Column()
  public quantity: number;

  @Column({ name: 'sb_type' })
  public sbType: string;

  @Column({ name: 'stop_price' })
  public stopPrice: number;

  @Column({ name: 'order_price' })
  public orderPrice: number;

  @Column({ name: 'order_type' })
  public orderType: string = StopOrderTypeEnum.STOP.valueOf();

  @Column({ name: 'user_id' })
  public userId: number;

  @Column({ name: 'service_code' })
  public serviceCode: string;

  @Column({ name: 'username' })
  public username: string;

  @Column({ name: 'account_number' })
  public accountNumber: string;

  @Column({ name: 'sub_number' })
  public subNumber: string;

  @Column({ name: 'order_number' })
  public orderNumber: string;

  @Column('json', { name: 'fail_reason' })
  public failReason: string;

  @Column({ name: 'connection_id' })
  public connectionId: string;

  @Column({ name: 'service_username' })
  public serviceUsername: string;

  @Column()
  public status: string = STOP_ORDER_STATUS.PENDING;

  @Column({ name: 'bank_code' })
  public bankCode: string;

  @Column({ name: 'bank_account' })
  public bankAccount: string;

  @Column({ name: 'bank_name' })
  public bankName: string;

  @Column({ name: 'securities_type' })
  public securitiesType: string;

  @Column({ name: 'created_by' })
  public createdBy: number;

  @Column({ name: 'created_at' })
  public createdAt: Date;

  @Column({ name: 'ordered_at' })
  public orderedAt: Date;

  @Column({ name: 'cancelled_at' })
  public cancelledAt: Date;

  @Column({ name: 'token', type: 'json' })
  public token: string;

  @Column({ name: 'source_ip' })
  public sourceIp: string;

  @Column({ name: 'from_date' })
  public fromDate: Date;

  @Column({ name: 'to_date' })
  public toDate: Date;

  @Column({ name: 'request_id' })
  public requestId: string;

  @Column({ name: 'otp_code' })
  public otpCode: string;

  @Column({ name: 'transaction_id' })
  public transactionID: number;

  @Column({ name: 'channel_name' })
  public channelName: string;

  @Column({ name: 'token_id' })
  public tokenId: string;

  @Column({ name: 'pin_code' })
  public pinCode: string;

  @Column({ name: 'thumb_print' })
  public thumbPrint: string;

  @Column({ name: 'serial_number' })
  public serialNumber: string;

  @Column({ name: 'remember_2_factor' })
  public remember2Factor: boolean;
}
