import AccountInfo from '../db/AccountInfo';

class LoginTuxedoResponse {
  public condId: number;
  public userInfo: UserInfo;
  public otpIndex: number;
  public otpValue: string;
  public userData: UserData;

  public toAccountInfoList(): AccountInfo[] {
    const accountInfoList: AccountInfo[] = [];
    const accounts: Account[] = this.userInfo.accounts;
    for (let i = 0; i < accounts.length; i++) {
      const account: Account = accounts[i];
      for (let j = 0; j < account.accountSubs.length; j++) {
        const accountSub: AccountSub = account.accountSubs[j];
        const accountInfo = new AccountInfo();
        accountInfo.username = this.userInfo.username;
        accountInfo.accountNumber = account.accountNumber;
        accountInfo.subNumber = accountSub.subNumber;
        accountInfo.bankCode = accountSub.bankAccounts[0].bankCode;
        accountInfo.bankName = accountSub.bankAccounts[0].bankName;
        accountInfo.identifierNumber = this.userData.identifierNumber;
        accountInfo.branchCode = this.userData.branchCode;
        accountInfo.mngDeptCode = this.userData.mngDeptCode;
        accountInfo.deptCode = this.userData.deptCode;
        accountInfo.agencyNumber = this.userData.agencyNumber;
        accountInfo.userType = this.userData.userType;
        accountInfoList.push(accountInfo);
      }
    }
    return accountInfoList;
  }
}

class UserInfo {
  public username: string;
  public identifierNumber: string;
  public accounts: Account[];
}

class Account {
  public accountNumber: string;
  public accountName: string;
  public accountSubs: AccountSub[];
}

class AccountSub {
  public subNumber: string;
  public bankAccounts: Bank[];
}

class Bank {
  public bankCode: string;
  public bankName: string;
}

class UserData {
  public username: string;
  public identifierNumber: string;
  public branchCode: string;
  public mngDeptCode: string;
  public deptCode: string;
  public agencyNumber: string;
  public userType: string;
}

export { LoginTuxedoResponse, UserData, UserInfo, Account, AccountSub, Bank };
