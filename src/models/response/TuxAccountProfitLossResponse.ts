export default class TuxAccountProfitLossResponse {
  public t1Deposit: number;
  public t2Deposit: number;
  public depositAmount: number;
  public totalBuyAmount: number;
  public totalEvaluationAmount: number;
  public totalProfitLoss: number;
  public totalProfitLossRate: number;
  public netAsset: number;
  public profitLossItems: ProfitLossItem[];
}

export class ProfitLossItem {
  public stockCode: string;
  public todayBuy: number;
  public todaySell: number;
  public balanceQuantity: number;
}
