export default class OrderHistoryResponse {
  public accountNumber: string;

  public subNumber: string;

  public stockCode: string;

  public orderDate: string;

  public orderTime: string;

  public sellBuyType: string;

  public orderType: string;

  public orderQuantity: number;

  public orderPrice: number;

  public matchedQuantity: number;

  public matchedPrice: number;

  public matchedAmount: number;

  public unMatchedQuantity: number;

  public modifyCancelType: string;

  public modifyCancelQuantity: number;

  public orderStatus: string;

  public orderNumber: string;

  public originalOrderNumber: string;

  public username: string;

  public branchCode: string;

  public deleted: boolean = false;
}
