import DailyProfitTotal from '../db/DailyProfitTotal';
import { Utils } from 'tradex-common';

export const parseToDailyProfitResponse = (
  dailyProfitTotal: DailyProfitTotal
): DailyProfitResponse => {
  const response: DailyProfitResponse = new DailyProfitResponse();
  if (!Utils.isNullOrUndefined(dailyProfitTotal)) {
    response.date = Utils.formatDateToDisplay(dailyProfitTotal.date);
    response.sellingAmount = Utils.round(dailyProfitTotal.sellAmount);
    response.buyingAmount = Utils.round(dailyProfitTotal.buyAmount);
    response.totalSellingAmount = Utils.round(
      dailyProfitTotal.totalSellingAmount
    );
    response.totalBuyingAmount = Utils.round(
      dailyProfitTotal.totalBuyingAmount
    );
    response.sellingProfit = Utils.round(dailyProfitTotal.sellingProfit);
    response.buyingProfit = Utils.round(dailyProfitTotal.buyingProfit);
    response.totalSellingProfit = Utils.round(
      dailyProfitTotal.totalSellingProfit
    );
    response.totalBuyingProfit = Utils.round(
      dailyProfitTotal.totalBuyingProfit
    );
    response.evaluatedAmount = Utils.round(
      dailyProfitTotal.todayEvaluatedAmount
    );
    response.priorEvaluatedAmount = Utils.round(
      dailyProfitTotal.yesterdayEvaluatedAmount
    );
    response.stockBalanceProfit = Utils.round(dailyProfitTotal.balanceProfit);
    response.totalStockBalanceProfit = Utils.round(
      dailyProfitTotal.totalStockBalanceProfit
    );
    response.dailyProfit = Utils.round(dailyProfitTotal.dailyProfit);
    response.totalProfit = Utils.round(dailyProfitTotal.totalProfit);
    response.profitRatio = Utils.round(dailyProfitTotal.profitRatio);
  }
  return response;
};

export default class DailyProfitResponse {
  public date: string;
  public sellingAmount: number;
  public buyingAmount: number;
  public totalSellingAmount: number;
  public totalBuyingAmount: number;
  public sellingProfit: number;
  public buyingProfit: number;
  public totalSellingProfit: number;
  public totalBuyingProfit: number;
  public evaluatedAmount: number;
  public priorEvaluatedAmount: number;
  public stockBalanceProfit: number;
  public totalStockBalanceProfit: number;
  public dailyProfit: number;
  public totalProfit: number;
  public profitRatio: number;
}
