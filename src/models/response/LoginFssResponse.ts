import { Models } from 'tradex-common';

export const USER_TYPE = {
  CUSTOMER: 'CUSTOMER',
  BROKER: 'BROKER',
};

export type UserType = typeof USER_TYPE[keyof typeof USER_TYPE];

export const ORDER_PASS_TYPE = {
  PIN: 'PIN',
  OTP: 'OTP',
  CA: 'CA',
};

export type OrderPassType = typeof ORDER_PASS_TYPE[keyof typeof ORDER_PASS_TYPE];

export const ACCOUNT_TYPE = {
  EQUITY: 'EQUITY',
  DERIVATIVES: 'DERIVATIVES',
};

export type AccountType = typeof ACCOUNT_TYPE[keyof typeof ACCOUNT_TYPE];

export interface IUserBankAccount {
  bankCode: string;
  bankName: string;
}

export interface IUserSubAccount {
  subNumber: string;
  bankAccounts: IUserBankAccount[];
  // tslint:disable-next-line:no-reserved-keywords
  type: AccountType;
}

export interface IUserAccount {
  accountNumber: string;
  accountName: string;
  accountDesc: string;
  autoAdvance: string;
  accountSubs: IUserSubAccount[];
}

export interface IUserInfo {
  username: string;
  orderPassType?: OrderPassType;
  identifierNumber: string;
  userType?: UserType;
  accounts: IUserAccount[];
}

export interface ILoginFssResponse {
  userInfo: IUserInfo;
  otpIndex?: string | number;
  otpValue?: string;
  userData: Models.IUserData;
}
