import { TradexNotification, Utils } from 'tradex-common';
import { MARKET_TIMEZONE } from '../../constants';

export default class OrderMatchNotification
  implements TradexNotification.ITemplateData {
  public orderNumber: string;
  public accountNumber: string;
  public subNumber: string;
  public code: string;
  public sellBuyType: string;
  public matchPrice: string;
  public matchQuantity: string;
  public time: string;
  public method: string = 'ORDER_MATCH';

  public getTemplate(): string {
    return 'order_match_notification';
  }
}

export class OrderMatchResponse {
  public orderNumber: string;
  public originalOrderNumber: string;
  public username: string;
  public accountNumber: string;
  public subNumber: string;
  public code: string;
  public sellBuyType: string;
  public orderPrice: number;
  public orderQuantity: number;
  public matchPrice: number;
  public matchQuantity: number;
  public totalQuantity: number;
  public unmatchQuantity: number;
  public marketType: string;
  public time: string;
  public execNo: string;
  public domain: string;
}

export const parse = (
  orderMatchResponse: OrderMatchResponse
): OrderMatchNotification => {
  const response: OrderMatchNotification = new OrderMatchNotification();
  response.orderNumber = orderMatchResponse.orderNumber;
  response.sellBuyType = orderMatchResponse.sellBuyType;
  response.matchQuantity = `${orderMatchResponse.matchQuantity}`;
  response.matchPrice = formatNumber(orderMatchResponse.matchPrice);
  response.time = convertTime(orderMatchResponse.time);
  response.accountNumber = orderMatchResponse.accountNumber;
  response.subNumber = orderMatchResponse.subNumber;
  response.code = orderMatchResponse.code;
  return response;
};

const formatNumber = (num: number): string => {
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
};

const convertTime = (time: string): string => {
  const dateTimeStr: string = `${Utils.formatDateToDisplay(new Date())}${time}`;
  const dateTime: Date = Utils.convertStringToDate(
    dateTimeStr,
    Utils.DATETIME_DISPLAY_FORMAT
  );
  dateTime.setHours(dateTime.getHours() + MARKET_TIMEZONE);
  return Utils.formatDateToDisplay(dateTime, 'kk:mm:ss');
};
