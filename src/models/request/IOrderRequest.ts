import { Models } from 'tradex-common';

export default interface IOrderRequest extends Models.IDataRequest {
  accountNumber: string;
  subNumber: string;
  stockCode: string;
  orderQuantity: number;
  orderPrice: number;
  sellBuyType: string;
  orderType: string;
  bankCode: string;
  bankAccount: string;
  bankName: string;
  securitiesType: string;
}
