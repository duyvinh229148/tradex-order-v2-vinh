import { Models } from 'tradex-common';

export default interface IStopOrderCancelAllRequest
  extends Models.IDataRequest {
  accountNumber: string;
  subNumber: string;
  sellBuyType?: string;
  orderType?: string;
  bankCode?: string;
  bankAccount?: string;
  stockCode?: string;
  stopPrice?: number;
}
