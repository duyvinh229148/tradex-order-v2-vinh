import { Models } from 'tradex-common';

export default interface IStopOrderCancelRequest extends Models.IDataRequest {
  sequence: number;
}
