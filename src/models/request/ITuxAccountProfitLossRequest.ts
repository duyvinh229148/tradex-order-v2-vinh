import { Models } from 'tradex-common';

export default interface ITuxAccountProfitLossRequest
  extends Models.IDataRequest {
  accountNumber: string;
  subNumber: string;
  bankCode: string;
  bankName: string;
  lastStockCode: string;
  fetchCount: number;
}
