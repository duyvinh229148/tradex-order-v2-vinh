import { Models } from 'tradex-common';

export default interface IStopOrderActiveRequest extends Models.IDataRequest {
  lastStopOrderId: number;
  fetchCount: number;
}
