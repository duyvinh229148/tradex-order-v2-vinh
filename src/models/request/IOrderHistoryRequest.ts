import { Models } from 'tradex-common';

export default interface IOrderHistoryRequest extends Models.IDataRequest {
  accountNumber: string;
  subNumber: string;
  fetchCount: number;
  lastOrderDate: string;
  lastBranchCode: string;
  lastOrderNumber: string;
  lastMatchPrice: number;
  sortType: string;
  fromDate: string;
  toDate: string;
}
