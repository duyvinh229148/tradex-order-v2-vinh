import { Models } from 'tradex-common';

export default interface IFssOrderRequest extends Models.IDataRequest {
  accountNumber: string;
  requestId?: string; //optional. Use to protect sending duplicate
  orderQuantity: number;
  orderPrice?: number;
  stockCode: string;
  sellBuyType: string;
  orderType: string;
  // otp
  // otpCode?: string;
  // transactionID?: number;
  // channelName?: string;
  // tokenId?: string;
  // // pin
  // pinCode?: string;
  // // ca
  // thumbPrint?: string;
  // serialNumber?: string;
  // remember2Factor?: boolean;
}
