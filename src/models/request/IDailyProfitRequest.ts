import { Models } from 'tradex-common';

export default interface IDailyProfitRequest extends Models.IDataRequest {
  accountNumber: string;
  subNumber?: string;
  baseDate?: string;
  fetchCount: number;
}
