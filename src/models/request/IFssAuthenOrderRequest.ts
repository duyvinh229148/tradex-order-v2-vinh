import {
  FssStopOrderPlaceRequest,
  FssStopOrderModifyRequest,
  FssStopOrderCancelRequest,
  FssStopOrderCancelMultiRequest,
} from 'tradex-models-order';
import { BaseRequest } from 'tradex-models-common';

export default interface IFssAuthenOrderRequest extends BaseRequest {
  // otp
  otpCode?: string;
  transactionID: number;
  channelName: string;
  tokenId: string;
  // pin
  pinCode?: string;
  // ca
  thumbPrint?: string;
  serialNumber?: string;
  remember2Factor?: boolean;
}

export const parseFromFssOrderPlaceRequest = (
  request: FssStopOrderPlaceRequest
): IFssAuthenOrderRequest => {
  return {
    otpCode: request.otpCode,
    transactionID: request.transactionID,
    channelName: request.channelName,
    tokenId: request.tokenId,
    pinCode: request.pinCode,
    thumbPrint: request.thumbPrint,
    serialNumber: request.serialNumber,
    remember2Factor: request.remember2Factor,
    headers: request.headers,
  };
};
export const parseFromFssOrderModifyRequest = (
  request: FssStopOrderModifyRequest
): IFssAuthenOrderRequest => {
  return {
    otpCode: request.otpCode,
    transactionID: request.transactionID,
    channelName: request.channelName,
    tokenId: request.tokenId,
    pinCode: request.pinCode,
    thumbPrint: request.thumbPrint,
    serialNumber: request.serialNumber,
    remember2Factor: request.remember2Factor,
    headers: request.headers,
  };
};

export const parseFromFssOrderCancelRequest = (
  request: FssStopOrderCancelRequest
): IFssAuthenOrderRequest => {
  return {
    otpCode: request.otpCode,
    transactionID: request.transactionID,
    channelName: request.channelName,
    tokenId: request.tokenId,
    pinCode: request.pinCode,
    thumbPrint: request.thumbPrint,
    serialNumber: request.serialNumber,
    remember2Factor: request.remember2Factor,
    headers: request.headers,
  };
};

export const parseFromFssOrderCancelMultiRequest = (
  request: FssStopOrderCancelMultiRequest
): IFssAuthenOrderRequest => {
  return {
    otpCode: request.otpCode,
    transactionID: request.transactionID,
    channelName: request.channelName,
    tokenId: request.tokenId,
    pinCode: request.pinCode,
    thumbPrint: request.thumbPrint,
    serialNumber: request.serialNumber,
    remember2Factor: request.remember2Factor,
    headers: request.headers,
  };
};
