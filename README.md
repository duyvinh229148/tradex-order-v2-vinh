### Clear npm cached
npm cache clean --force

#### Update model from jsonSchema
npm install --save https://s3-ap-southeast-1.amazonaws.com/tradex-dev/tradex-models/typescript/tradex-models-common-1.0.0.tgz

npm install --save https://s3-ap-southeast-1.amazonaws.com/tradex-dev/tradex-models/typescript/tradex-models-order-1.0.0.tgz

npm install --save https://s3-ap-southeast-1.amazonaws.com/tradex-dev/tradex-models/typescript/tradex-models-order-validator-1.0.0.tgz
