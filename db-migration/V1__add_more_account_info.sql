ALTER TABLE `tradex-order`.`t_account_info` 
ADD COLUMN `bank_code` varchar(25) NULL AFTER `updated_at`,
ADD COLUMN `bank_name` varchar(25) NULL AFTER `bank_code`,
ADD COLUMN `branch_code` varchar(25) NULL AFTER `bank_name`,
ADD COLUMN `identifier_number` varchar(25) NULL AFTER `branch_code`,
ADD COLUMN `mng_dept_code` varchar(25) NULL AFTER `identifier_number`,
ADD COLUMN `dept_code` varchar(25) NULL AFTER `mng_dept_code`,
ADD COLUMN `agency_number` varchar(25) NULL AFTER `dept_code`,
ADD COLUMN `user_type` varchar(25) NULL AFTER `agency_number`;