ALTER TABLE `tradex-order`.`t_stop_order`
MODIFY COLUMN `securities_type` enum('STOCK','FUND','BOND','ETF','CW','FUTURES') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL AFTER `bank_name`;

ALTER TABLE `tradex-order`.`t_stop_order`
ADD COLUMN `request_id` varchar(255) NULL AFTER `to_date`,
ADD COLUMN `otp_code` varchar(255) NULL AFTER `request_id`,
ADD COLUMN `transaction_id` bigint(0) NULL AFTER `otp_code`,
ADD COLUMN `channel_name` varchar(255) NULL AFTER `transaction_id`,
ADD COLUMN `token_id` varchar(255) NULL AFTER `channel_name`,
ADD COLUMN `pin_code` varchar(255) NULL AFTER `token_id`,
ADD COLUMN `thumb_print` varchar(255) NULL AFTER `pin_code`,
ADD COLUMN `serial_number` varchar(255) NULL AFTER `thumb_print`,
ADD COLUMN `remember_2_factor` bit(1) NULL AFTER `serial_number`;
