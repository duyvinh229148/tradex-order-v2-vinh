INSERT INTO `tradex-configuration`.t_scope(id, name, uri_pattern, forward_type, forward_data)VALUES(273, 'MARKET_INDEX_STOCK_LIST', 'get:/api/v1/market/indexStockList/{indexCode}', 'CONNECTION', '{"service":"market","uri":"/api/v1/market/indexStockList/{indexCode}","forwardType":"SERVICE"}');
INSERT INTO `tradex-configuration`.t_scope_scope_group_map (scope_id,scope_group_id)VALUES(273, 4);


INSERT INTO `tradex-configuration`.t_scope_group(id, scope_group_name, created_by, updated_by, created_at, updated_at)VALUES(18, 'BASKET_ORDER', NULL, NULL, TIMESTAMP '2019-01-15 13:59:28', TIMESTAMP '2019-01-15 13:59:28');

INSERT INTO `tradex-configuration`.t_scope(id, name, uri_pattern, forward_type, forward_data)VALUES(274, 'BASKET_ORDER_QUERY_ORDER_DRAFT', 'get:/api/v1/equity/order/basketOrder/draft', 'CONNECTION', '{"service":"order","uri":"/api/v1/equity/order/basketOrder/draft","forwardType":"SERVICE"}');
INSERT INTO `tradex-configuration`.t_scope(id, name, uri_pattern, forward_type, forward_data)VALUES(275, 'BASKET_ORDER_SUBMIT', 'post:/api/v1/equity/order/basketOrder/submit', 'CONNECTION', '{"service":"order","uri":"/api/v1/equity/order/basketOrder/submit","forwardType":"SERVICE"}');
INSERT INTO `tradex-configuration`.t_scope(id, name, uri_pattern, forward_type, forward_data)VALUES(277, 'BASKET_ORDER_DRAFT_UPDATE', 'put:/api/v1/equity/order/basketOrder/draft/update', 'CONNECTION', '{"service":"order","uri":"/api/v1/equity/order/basketOrder/draft/update","forwardType":"SERVICE"}');
INSERT INTO `tradex-configuration`.t_scope(id, name, uri_pattern, forward_type, forward_data)VALUES(278, 'BASKET_ORDER_CANCEL', 'put:/api/v1/equity/order/basketOrder/cancel', 'CONNECTION', '{"service":"order","uri":"/api/v1/equity/order/basketOrder/cancel","forwardType":"SERVICE"}');
INSERT INTO `tradex-configuration`.t_scope(id, name, uri_pattern, forward_type, forward_data)VALUES(279, 'BASKET_ORDER_HIST', 'get:/api/v1/equity/order/basketOrder/history', 'CONNECTION', '{"service":"order","uri":"/api/v1/equity/order/basketOrder/history","forwardType":"SERVICE"}');
INSERT INTO `tradex-configuration`.t_scope(id, name, uri_pattern, forward_type, forward_data)VALUES(280, 'BASKET_ORDER_ORDER_CANCEL', 'put:/api/v1/equity/order/basketOrder/order/cancel', 'CONNECTION', '{"service":"order","uri":"/api/v1/equity/order/basketOrder/order/cancel","forwardType":"SERVICE"}');
INSERT INTO `tradex-configuration`.t_scope(id, name, uri_pattern, forward_type, forward_data)VALUES(281, 'BASKET_ORDER_ORDER_HISTORY', 'get:/api/v1/equity/order/basketOrder/order/history', 'CONNECTION', '{"service":"order","uri":"/api/v1/equity/order/basketOrder/order/history","forwardType":"SERVICE"}');
INSERT INTO `tradex-configuration`.t_scope(id, name, uri_pattern, forward_type, forward_data)VALUES(282, 'BASKET_ORDER_ORDER_MODIFY', 'put:/api/v1/equity/order/basketOrder/order/modify', 'CONNECTION', '{"service":"order","uri":"/api/v1/equity/order/basketOrder/order/modify","forwardType":"SERVICE"}');


INSERT INTO `tradex-configuration`.t_scope_scope_group_map (scope_id,scope_group_id)VALUES(274, 18);
INSERT INTO `tradex-configuration`.t_scope_scope_group_map (scope_id,scope_group_id)VALUES(275, 18);
INSERT INTO `tradex-configuration`.t_scope_scope_group_map (scope_id,scope_group_id)VALUES(276, 18);
INSERT INTO `tradex-configuration`.t_scope_scope_group_map (scope_id,scope_group_id)VALUES(277, 18);
INSERT INTO `tradex-configuration`.t_scope_scope_group_map (scope_id,scope_group_id)VALUES(278, 18);
INSERT INTO `tradex-configuration`.t_scope_scope_group_map (scope_id,scope_group_id)VALUES(279, 18);
INSERT INTO `tradex-configuration`.t_scope_scope_group_map (scope_id,scope_group_id)VALUES(280, 18);
INSERT INTO `tradex-configuration`.t_scope_scope_group_map (scope_id,scope_group_id)VALUES(281, 18);
INSERT INTO `tradex-configuration`.t_scope_scope_group_map (scope_id,scope_group_id)VALUES(282, 18);





