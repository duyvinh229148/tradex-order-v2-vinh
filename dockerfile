FROM localhost:5000/alpine-node-rd-kafka:8.11.3
RUN mkdir -p /app
RUN mkdir -p /app/lib
COPY / /app
COPY /tradex-common/ /app/lib/tradex-common
WORKDIR /app
RUN npm install
RUN npm run build
RUN cp env.js /app/build/src/env.js
WORKDIR /app/build/src
ENTRYPOINT [ "node", "index.js" ]